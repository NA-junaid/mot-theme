const UrlService = {
	getHome: "home",
	getSetup: "setup",
	getPage: "page",

	getLogin: "login",

	//get routes,
	getUser:				'user',
	getaccountIndex:		'account/index',
	getAccountEdit:		    'account/edit',
	getAccountPassword:		'account/password/edit',

	getOrderIndex:		    'order/index',
	getAddressIndex:		'address/index',
	getAddressDelete:		'address/delete',
	getWishlistIndex:		'wishlist/index',
	getOrderReturn:		    'order/return',
	
	// need to pass the id of required actions with {id} url
	getAddressEdit:		    'address/edit/{id}',
	getOrderCancel:		    'order/cancel/{id}',
	getOrderList:		    'order/{id}',
	
	getStoreLocator:  		'page/store_locator',
	
	//post routes,
	postWishlistToggle:		'wishlist/toggle',
	postAddressAdd:		    'address/add',
	postAddressUpdate:		'address/update',
	postAccountUpdate:		'account/update',
	postAccountPassword:	'account/password/update',
	postOrderSave:		    'order/save',
	
	postLogin: 				'login',
	postRegister:  			'register',

	postContactUs:  		'pages/contact-us',
	postSubscribeUser:  	'subscribe_user',

	//need to pass order no with order_no keyword like track_order?order_no=TES00541
	postTrackOrder:  		 'track_order',



};

const urlService = UrlService;

export default urlService;
