import axios from "axios";
import qs from "qs";

const api = "https://services.nextaxe.com/api/";
// const api = 'http://localhost:8001/api/';

class NetworkService {
  get(url, data) {
    return this.executeRequest(url, data, "GET");
  }

  post(url, data) {
    return this.executeRequest(url, data, "POST");
  }

  async executeRequest(url, data, type) {

    const uri = `${api}${url}?store_id=178`;

    let options = {
      method: type,
      url: uri,
      data: null,
    };
    
    if (process.browser) {
      if (localStorage != undefined) {
        const token = localStorage.getItem("token");

        if (token != null) {
          options.headers = {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
        }
      }
    }
  }

    if (type == "GET") {
      if (data != undefined || data != null) {
        options.params = data;
      }
    } else if (type == "POST") {
      if (data != undefined || data != null) {
        options.data = qs.stringify(data);
      }
    }

    return axios(options)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

const networkService = new NetworkService();

export default networkService;
