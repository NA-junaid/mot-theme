import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addPage } from "../redux/actions/page-action";
import { addTranslation } from "../redux/actions/translation-action";
import { addCategory } from "../redux/actions/category-action";
import { addLanguage } from "../redux/actions/language-action";
import { setStore } from "../redux/actions/store-action";
import { addPaymentMethod } from "../redux/actions/payment-method-actions";
import { addShippingMethod } from "../redux/actions/shipping-method-actions";
import { setWebSetting } from "../redux/actions/web-setting-action";
import { setSocialLinks } from "../redux/actions/social-links-action";
import { addCurrency } from "../redux/actions/currency-action";
import { addOutlet } from "../redux/actions/outlet-action";
import { addPlugin } from "../redux/actions/plugin-action";
import { addZone } from "../redux/actions/zone-action";
import { addArea } from "../redux/actions/area-action";
import { saveState } from "../services/storage";
import store from "../redux/index";

const reduxStore = store;

let initialState = {
  languages: [],
  translations: [],
  pages: [],
  categories: [],
  store: {},
  payment_methods: [],
  shipping_methods: [],
  web_setting: {},
  social_links: [],
};

function useSetup(props) {
  const dispatch = useDispatch();
  const [setup, setSetup] = useState({});

  const initialize = async () => {
    const {
      pages,
      translations,
      categories,
      languages,
      store,
      payment_methods,
      shipping_methods,
      web_setting,
      web_social_link,
      currencies,
      outlets,
      store_plugin,
      zones_list,
      areas_list,
    } = props;

    if (pages.length > 0) {
      pages.map((page) => {
        dispatch(addPage(page));
      });
    }

    if (translations != null) {
      dispatch(addTranslation(translations));
    }

    if (categories.length > 0) {
      categories.map((category) => {
        dispatch(addCategory(category));
      });
    }

    if (languages.length > 0) {
      languages.map((language, index) => {
        language.isDefault = index == 0 ? true : false;
        dispatch(addLanguage(language));
      });
    }

    if (payment_methods.length > 0) {
      payment_methods.map((payment_method) => {
        dispatch(addPaymentMethod(payment_method));
      });
    }

    if (shipping_methods.length > 0) {
      shipping_methods.map((shipping_method) => {
        dispatch(addShippingMethod(shipping_method));
      });
    }

    if (store != null) {
      dispatch(setStore(store));
    }
    if (web_setting != null) {
      dispatch(setWebSetting(web_setting));
    }

    if (web_social_link != null) {
      web_social_link.map((social_link) => {
        dispatch(setSocialLinks(social_link));
      });
    }

    if (currencies!=null && currencies.length > 0) {
      currencies.map((currency, index) => {
        dispatch(
          addCurrency({ name: currency, isDefault: index == 0 ? true : false })
        );
      });
    }

    if (outlets.length > 0) {
      outlets.map((outlet) => {
        dispatch(addOutlet(outlet));
      });
    }

    if (store_plugin.length > 0) {
      store_plugin.map((plugin) => {
        dispatch(addPlugin(plugin));
      });
    }

    if (zones_list.length > 0) {
      zones_list.map((zone) => {
        dispatch(addZone(zone));
      });
    }

    if (areas_list.length > 0) {
      areas_list.map((area) => {
        dispatch(addArea(area));
      });
    }

    saveState("state",reduxStore.getState());
  };

  useEffect(() => {
    initialize();
  });
  return [setup, setSetup];
}

export default useSetup;
