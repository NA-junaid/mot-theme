import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { saveState,loadState } from "../services/storage";
import {
  addToCart,
  removeFromCart,
  decreaseQuantity,
  increaseQuantity,
} from "../redux/actions/cartAction";
import store from "../redux/index";

const reduxStore = store;


function useCart() {
  const [value, setVaue] = useState({
    quantity: 0,
    product: {},
    increment: false,
  });
  const cart = useSelector((state) => state.cart);

  const store = useSelector((state) => state.store);
  const dispatch = useDispatch();

  const updateState = () =>{
    saveState("state",reduxStore.getState());
  }

  useEffect(() => {
    const { quantity, product, increment } = value;

    product.round_off = store.round_off;

    if (Object.keys(product).length > 0) {
      if (quantity == 0) {
        dispatch(removeFromCart(product));
      } else if (quantity == 1) {
        dispatch(addToCart(product));
      } else {
        if (increment) {
          dispatch(increaseQuantity(product));
        } else {
          dispatch(decreaseQuantity(product));
        }
      }
    }
    updateState();
  }, [value]);

  return [setVaue];
}

export default useCart;
