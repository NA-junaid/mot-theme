import { useState, useEffect } from "react";
// import {  useDispatch } from "react-redux";
import { saveState, loadState } from "../services/storage";

const init = {
  name: "",
  email: "",
  phone: "",
  pickup: false,
  outlet: null,
  area: null,
  paymentMethod: null,
  shippingMethod: null,
  shippingRate: 0,
};

let persistedState = loadState("checkout");

if (
  persistedState == null ||
  persistedState == undefined ||
  persistedState == {}
) {
  persistedState = init;
}

function useCheckoutDataHolder() {
  const [data, setData] = useState(persistedState);
  const { outlet, area, pickup } = data;
  //const dispatch = useDispatch();

  useEffect(() => {
    // console.log('data',data);
    saveState("checkout", data);
  }, [outlet, area, pickup, data]);

  return [data, setData];
}

export default useCheckoutDataHolder;
