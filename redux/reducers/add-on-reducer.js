const initState = [];

const addonReducer = (state = initState, action) => {
  switch (action.type) {
    /* Add addon in list */
    case "ADD_ADDON": {
      if (action.payload.group) {
        let selectedGroup = state.find(
          (i) => i.grounName == action.payload.grounName
        );

        let list = [
          ...state.filter((i) => !(i.grounName == action.payload.grounName)),
        ];

        return [...list, action.payload];
      } else {
        let selectedAddon = state.find(
          (i) =>
            i.name == action.payload.name && i.price == action.payload.price
        );

        if (selectedAddon != undefined) {
          return state;
        } else {
          return [...state, action.payload];
        }
      }
    }
    /* remove item from addons */
    case "REOMVE_ADDON": {
      return [
        ...state.filter(
          (i) =>
            !(i.name == action.payload.name && i.price == action.payload.price)
        ),
      ];
    }

    case "DISCARD_ADDONS": {
      return [];
    }
    default:
      return state;
  }
};

export default addonReducer;
