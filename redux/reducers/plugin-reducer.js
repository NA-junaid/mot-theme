const pluginItem = (state, action) => {
    switch (action.type) {
        case 'ADD_PLUGIN':
            return action.payload;
        default:
            return state;
    }
}

export default function pluginReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_PLUGIN':
            let item = state.find(item =>  item.id == action.payload.id);
            if (item == undefined) {
                return [
                    ...state,
                    pluginItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}