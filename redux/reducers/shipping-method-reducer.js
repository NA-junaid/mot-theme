const shippingMethodItem = (state, action) => {
    switch (action.type) {
        case 'ADD_SHIPPING_METHOD':
            return action.payload;
        default:
            return state;
    }
}

export default function shippingMethodReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_SHIPPING_METHOD':
            let item = state.find(item =>  item.name == action.payload.name);
            if (item == undefined) {
                return [
                    ...state,
                    shippingMethodItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}