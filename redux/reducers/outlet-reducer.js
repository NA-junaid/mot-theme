const outletItem = (state, action) => {
    switch (action.type) {
        case 'ADD_OUTLET':
            return action.payload;
        default:
            return state;
    }
}

export default function outletReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_OUTLET':
            let item = state.find(item =>  item.id == action.payload.id);
            if (item == undefined) {
                return [
                    ...state,
                    outletItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}