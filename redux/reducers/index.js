import { combineReducers } from 'redux';

import languageReducer from './language-reducer';
import pageReducer from './page-reducer';
import categoryReducer from './category-reducer';
import storeReducer from './store-reducer';
import translationReducer from './translation-reducer';
import paymentMethodReducer from './payment-method-reducer';
import shippingMethodReducer from './shipping-method-reducer';
import webSettingReducer from './web-setting-reducer';
import socialLinksReducer from './social-links-reducer';
import currencyReducer from './currency-reducer';
import outletReducer from './outlet-reducer';
import pluginReducer from './plugin-reducer';
import areaReducer from './area-reducer';
import zoneReducer from './zone-reducer';
import popupReducer from './popup-reducer';
import cartReducer from './cart-reducer';
import pickUpReducer from './pick-up-reducer';
import checkoutStepReducer from './check-out-step-reducer';
import addonReducer from './add-on-reducer';

export default combineReducers({
    languages: languageReducer,
    pages: pageReducer,
    categories: categoryReducer,
    store: storeReducer,
    translations:translationReducer,
    paymentMethods:paymentMethodReducer,
    shippingMethods:shippingMethodReducer,
    webSetting: webSettingReducer,
    socialLinks: socialLinksReducer,
    currencies: currencyReducer,
    outlets : outletReducer,
    plugins : pluginReducer,
    areas : areaReducer,
    zones : zoneReducer,
    popup : popupReducer,
    cart : cartReducer,
    pickUp : pickUpReducer,
    checkoutStep : checkoutStepReducer,
    addons : addonReducer
});