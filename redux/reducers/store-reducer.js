const initialState = {  };

export default function storeReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_STORE":
      return action.payload;
    default:
      return state;
  }
}
