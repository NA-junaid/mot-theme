const initialState = {  };

export default function translationReducer(state = initialState, action) {
  switch (action.type) {
    case "ADD_TRANSLATION":
      return action.payload;
    default:
      return state;
  }
}
