import store from "../index";

const initState = {
  items: [],
  total: 0,
};

const cartItem = (state, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      let product = action.payload
      product.quantity = 1;
      product.retail_price = parseFloat(product.retail_price).toFixed(product.round_off);
      let total = parseFloat(product.retail_price);

      if(product.addons.length > 0){
        product.addons.map(addon => {
          total = parseFloat(total) + parseFloat(addon.price)
        })
      }
      product.total = parseFloat(product.quantity * total).toFixed(product.round_off);
      return product;
    default:
      return state;
  }
};

const addToCart = (state, action, lang) => {
  let product = action.payload;
  let item = state.items.find((item) => item.variant_id == product.variant_id && JSON.stringify(item.addons) == JSON.stringify(product.addons) );

  if (item == undefined) {
    let total = state.total;

    let productTotal = parseFloat(product.retail_price);

    if(product.addons.length > 0){
      product.addons.map(addon => {
        productTotal += parseFloat(addon.price);
      })
    }
    
    /* add to total with quantity = 1 */
    total =  (parseFloat(total) + parseFloat(productTotal)).toFixed(product.round_off);

    return {
      items: [...state.items, cartItem(undefined, action)],
      total: total,
    };
  } else {
    return state;
  }
};

const removeFromCart = (state, action, lang) => {
  let product = action.payload;
 
  let item = state.items.find((item) => item.variant_id == product.variant_id && JSON.stringify(item.addons) == JSON.stringify(product.addons));

  if (item != undefined) {

    let productTotal = parseFloat(item.retail_price);

    if(item.addons.length > 0){
      item.addons.map(addon => {
        productTotal += parseFloat(addon.price);
      })
    }

    let total = (parseFloat(state.total) - parseFloat(productTotal * item.quantity)).toFixed(product.round_off);
    let items = state.items.filter(
      (item) => item.variant_id != product.variant_id
    );

    return {
      items: [...items],
      total: total,
    };
  } else {
    return state;
  }
};

const increaseQuantity = (state, action, lang) => {
  let total = state.total;
  let product = action.payload;

  let items = state.items.map((item) => {
    if (item.variant_id == product.variant_id && JSON.stringify(item.addons) == JSON.stringify(product.addons)) {
      item.quantity = item.quantity + 1;

      let productTotal = parseFloat(item.retail_price);

      if(item.addons.length > 0){
        item.addons.map(addon => {
          productTotal += parseFloat(addon.price);
        })
      }

      item.total = parseFloat(productTotal * item.quantity).toFixed(product.round_off);
      total = (parseFloat(total) + parseFloat(productTotal)).toFixed(product.round_off);
    }
    return item;
  });

  return {
    items: [...items],
    total: total,
  };
};

const decreaseQuantity = (state, action, lang) => {

  let product = action.payload;
  let total = state.total;

  let items = state.items.map((item) => {
    if (item.variant_id == product.variant_id && JSON.stringify(item.addons) == JSON.stringify(product.addons)) {

      let productTotal = parseFloat(item.retail_price);

      if(item.addons.length > 0){
        item.addons.map(addon => {
          productTotal += parseFloat(addon.price);
        })
      }

      item.quantity = item.quantity - 1;
      item.total = parseFloat(productTotal * item.quantity).toFixed(product.round_off);
      total = (parseFloat(total) - parseFloat(productTotal)).toFixed(product.round_off);
    }
    return item;
  });

  return {
    items: [...items],
    total: total,
  };
};

const cartReducer = (state = initState, action) => {
  // const storeState = store.getState();
  // const languages = storeState.languages;
  // const filtered = languages.filter((item) => item.isDefault);
  // const currentLanguage = filtered[0];
  // const lang = currentLanguage.short_name;
  const lang = "en";

  switch (action.type) {
    /* Add item in cart item list with quantity 1 */
    case "ADD_TO_CART": {
      return addToCart(state, action, lang);
    }
    /* remove item from cart */
    case "REOMVE_FROM_CART": {
      return removeFromCart(state, action, lang);
    }

    case "INCREASE_QUANTITY": {
      return increaseQuantity(state, action, lang);
    }

    case "DECREASE_QUANTITY": {
      return decreaseQuantity(state, action, lang);
    }

    case "DISCARD_CART": {
      return {
        items: [],
        total: 0,
      };
    }
    default:
      return state;
  }
};

export default cartReducer;
