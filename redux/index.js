import { createStore } from 'redux';
import rootReducer from './reducers';
import { loadState } from '../services/storage';

const initialState = {};
let persistedState = loadState("state");

if(persistedState == null || persistedState == undefined || persistedState == {}){
    persistedState = {}
}

let store = createStore(rootReducer, persistedState);

export default store;
