export function setPickup(payload) {
    return {
        type: 'SET_PICKUP',
        payload
    }
};