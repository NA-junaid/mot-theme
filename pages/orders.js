import React from "react";
import {BreadcrumbsBar} from "../components/common";

import {OrderDetails,OrderCard,ShippingAddress} from "../components/partials/orders";

function Orders() {
  return (
    <div className="container-fluid">
      <BreadcrumbsBar/>
      {/*================= 
    Start Order Tracking  
    ==================*/}
      <div className="order-tracking mt-minus bg-white p-5">
        <OrderDetails />
        <OrderCard />
        <OrderCard />
      </div>
      <ShippingAddress />
    </div>

    /*================= 
  End Order Tracking
  ==================*/


  );
}

export default Orders;
