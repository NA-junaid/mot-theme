import Link from 'next/link';

import {DeliveryProcess,TopLinks,CategorySidebar,Slider,ThreeColumnAdvertisement,TodayDeals,ProductList,ProductSection,Banner} from '../components/partials/home';

import networkService from '../services/network-service';
import urlService from '../services/url-service';
import useSetup from '../services/use-setup';

function Home(props) {
  const [setup, setSetup] = useSetup(props);
  const { banners } = props;
  return (
    <div>
      <div className="row mt-4">
        <div className="col-md-9 offset-md-3">
          <DeliveryProcess />
          <TopLinks/>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row">
          <CategorySidebar/>
          <Slider banners={banners}/>
        </div>
      </div>
      <div>
        <ThreeColumnAdvertisement />
        <TodayDeals />
        <ProductList />
        <ThreeColumnAdvertisement />
        <ProductSection/>
        <Banner />
        <ProductSection />
        <ThreeColumnAdvertisement />
        <ProductList />
        <Banner />
        <ProductSection />
        <ThreeColumnAdvertisement />
        <ProductSection />
        <Banner />
      </div>
    </div>
  )
}
export async function getStaticProps() {
    let data = {
      banners: [],
      // products: []
    };
  
    const response = await networkService.get(urlService.getSetup);
  
    if (response.IsValid) {
      data = { ...response.Payload };
    }
  
    // Get external data from the file system, API, DB, etc.
    const homeResponse = await networkService.get(urlService.getHome);
    if (homeResponse.IsValid) {
      const  {banners} = homeResponse.Payload;  
      data.banners = banners;
      // data.products = products;
    }
    return {
      props: data,
    };
  }
export default Home
