import Link from "next/link";

import {BreadcrumbsBar,GridItem,} from "../components/common";
import {Banner,SearchFilters,Pagination,Filters} from "../components/partials/products";

export default function Products() {
  return (
    <div>
      <BreadcrumbsBar />
      <Banner />
      <SearchFilters/>
      <div className="container-fluid products_block">
        <div className="row no-gutters">
          <div className="col-md-3">
            <Filters />
          </div>
          <div className="col-md-9">
            <div className="row products_container">
              {
                [1,2,3,4,5,6,7,8,9,10,11,12].map((i)=>{
                  return <GridItem/>
                })
              }
            </div>
          </div>
        </div>
        <Pagination />
      </div>
    </div>
  );
}
