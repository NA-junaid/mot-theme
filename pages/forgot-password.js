import React from "react";
import {BreadcrumbsBar} from "../components/common";
import {NavigationItems,PasswordForgot,Authentication,ResetPassword} from "../components/partials/forgotpassword";

function ForgotPassword() {
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">
        <NavigationItems/>
        <div className="tab-content" id="pills-tabContent">
          <div
            className="tab-pane fade"
            id="forgotpass"
            role="tabpanel"
            aria-labelledby="forgotpass-tab"
          >
            <PasswordForgot />
          </div>
          <div
            className="tab-pane fade show active"
            id="athentication"
            role="tabpanel"
            aria-labelledby="athentication-tab"
          >
            <Authentication />
          </div>
          <div
            className="tab-pane fade "
            id="newpass"
            role="tabpanel"
            aria-labelledby="newpass-tab"
          >
            <ResetPassword />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
