import React, { Suspense } from "react";

import Link from "next/link";

import TopHeader from "../components/topheader";
import MainHeader from "../components/mainheader";
import { Newsletter, Backtop, Footer, PageHead } from "../components/common";
import store from "../redux/index";
import { Provider } from "react-redux";

export default function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout ? Component.Layout : React.Fragment;
  return (
    <Provider store={store}>
      <Layout>
        <PageHead />
        <TopHeader />
        <MainHeader />
        <div className="content">
          <Component {...pageProps} />
        </div>
        <Newsletter />
        <Backtop />
        <Footer />
      </Layout>
    </Provider>
  );
}

// <Suspense fallback={<div>Loading...</div>}>
// </Suspense>