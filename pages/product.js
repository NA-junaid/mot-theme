import {BreadcrumbsBar} from "../components/common";
import {ProductDetail,NavTabs,ShopInfo,AdditionalInfo,Comments,AddYourReview} from "../components/partials/productdetail";

import useTranslation from "../services/use-translation";
export default function Product() {
  const { buy, similar_products } = useTranslation();
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid product_detail">
        <ProductDetail />
        <div className="container">
          <div className="row mt-4  pb-3">
            <div className="col-md-12">
              <div className="tab-style3">
                <NavTabs />
                <div className="tab-content shop_info_tab">
                  <ShopInfo />
                  <AdditionalInfo />
                  <div className="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews-tab">
                    <Comments />
                    <AddYourReview />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="similar_products">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <h2 className="text-center mb-lg-4 mt-lg-4"><span className="tag_sm"><i className="icon-tag" /></span>{similar_products}</h2>
              <div className="row products_container">

                {
                  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(product => {
                    return (
                      <div className="col-md-3">
                        <div className="products_wrapper">
                          <div className="hover_state">
                            <h4>Fast Store</h4>
                            <div className="cart">
                    <a href="#"><i className="icon-basket" /> {buy}</a>
                              <a href="#" className="wishlist"><i className="icon-heart" /></a>
                              <h2>Jordan Galaxy</h2>
                              <h3 className="price mt-2">KD 280</h3>
                            </div>
                          </div>
                          <figure><img src="assets/img/products/img8.jpg" /></figure>
                          <h2>Jordan Galaxy</h2>
                          <h3 className="price mt-2">KD 280</h3>
                        </div>
                      </div>
                    )
                  })
                }

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}