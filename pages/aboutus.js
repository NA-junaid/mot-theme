import useTranslation from "../services/use-translation";
import { BreadcrumbsBar } from "../components/common";
import { About, WhyChoose, Service, ShopInfo } from "../components/partials/aboutus";

function AboutUs() {
    const { who_we_are } = useTranslation();
    return (
        <div>
            <BreadcrumbsBar />
            <div class="container-fluid">
                <div className="about-us bg-white p-5 mt-minus">
                    <div className="main_content">
                        {/* STAT SECTION ABOUT */}
                        <About text={who_we_are}/>
                        {/* END SECTION ABOUT */}
                        {/* START SECTION WHY CHOOSE */}
                        <WhyChoose />
                        {/* END SECTION WHY CHOOSE */}
                        <Service />
                        {/* START SECTION SHOP INFO */}
                        <ShopInfo />
                        {/* END SECTION SHOP INFO */}
                    </div>
                </div>

            </div>
        </div>
    );
}
export default AboutUs;