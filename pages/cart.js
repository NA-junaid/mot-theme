import React from "react";
import {BreadcrumbsBar} from "../components/common";
import {NavItems,ShippingMethod,Payment,CheckOut} from "../components/partials/cart";

function Cart() {
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">
        <NavItems />
        <div className="tab-content" id="pills-tabContent">
          <CheckOut />
          <ShippingMethod />
          <Payment />
        </div>
      </div>
    </div>

  );
}

export default Cart;