import React, { Component } from "react";
import useTranslation from "../services/use-translation";
 
function Stuff(){
    const {stuff} = useTranslation();
    return (
      <div>
        <h2>{stuff}</h2>
        <p>Mauris sem velit, vehicula eget sodales vitae,
        rhoncus eget sapien:</p>
        <ol>
          <li>Nulla pulvinar diam</li>
          <li>Facilisis bibendum</li>
          <li>Vestibulum vulputate</li>
          <li>Eget erat</li>
          <li>Id porttitor</li>
        </ol>
      </div>
    );
  }
 
export default Stuff;