import React from "react";
import useTranslation from "../services/use-translation";
import {BreadcrumbsBar} from "../components/common";

function EmptyPage() {
    const { cart_empty, no_item_in_cart, continue_shopping } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="about-us bg-white p-5 mt-minus">
                <div className="row align-items-center">
                    <div className="col-md-12 text-center">
                        <div className="empty-cart">
                            <span className="icon-basket " />
                            <h1 className="title mt-3 ">{cart_empty}</h1>
                            <p>{no_item_in_cart}</p>
                            <a href="#" className="mt-3 btn-primary">
                                {continue_shopping}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default EmptyPage;