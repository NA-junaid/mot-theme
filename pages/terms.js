import React from "react";

import {BreadcrumbsBar} from "../components/common";
import {Conditions,About,Usage} from "../components/partials/terms";
import useTranslation from "../services/use-translation";

function Terms() {
  const {terms,terms_where,terms_why} = useTranslation();
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">
        <div className="terms-conditions bg-white p-5 mt-minus">
          <div className="row justify-content-center align-items-center">
            <div className="col-md-12">
              <div className="term_conditions">
                <Conditions text={terms}/>
                <About text={terms_where}/>
                <Usage text={terms_why}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Terms;
