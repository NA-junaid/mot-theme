import React from "react";
import { BreadcrumbsBar } from "../components/common";
import useTranslation from "../services/use-translation";
import HistoryItems from "../components/partials/orderhistory/history-items";

function OrderHistory() {
    const { } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="bg-white pt-4 pl-5 pr-5 mt-minus">
                <div className="pb-5">
                    <div className="row">
                        <div className="col-lg-12 pl-4 pr-4 pt-4 bg-white rounded ">
                            {
                                [1, 2, 3, 4].map(product => {
                                    return (
                                        <HistoryItems />
                                    )
                                })
                            }

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default OrderHistory;