import React from "react";
import { BreadcrumbsBar } from "../components/common";
import useTranslation from "../services/use-translation";
import ReturnItemsList from "../components/partials/ReturnProduct/return-items-list";

function ReturnProduct() {
    const { _return } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="cart-main-area p-4 mt-minus bg-white">
                <div className="row">
                    <div className="col-md-12 col-sm-12">
                        <form action="#">
                            <ReturnItemsList/>
                            <div className="row mt-4">
                                <div className="col-md-12 col-sm-12 text-center">
                                    <button type="submit" value="Apply" className="btn btn-return">
                                        {_return}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>;


    </div>
}

export default ReturnProduct;