import React from "react";
import {BreadcrumbsBar} from "../components/common";
import {LoginForm,RegisterForm} from "../components/partials/login";
import useTranslation from "../services/use-translation";

function Login() {
  const {} = useTranslation();
  return (
    <div className="container-fluid">
      <BreadcrumbsBar />
      <div className="login-reg-cont bg-white mt-minus mb-5">
        <div className="row">
          <div className="col-md-6">
            <LoginForm />
          </div>
          <div className="col-md-6">
            <RegisterForm />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
