import React from "react";
import {BreadcrumbsBar} from "../components/common";
import useTranslation from "../services/use-translation";

function FAQS() {
    const { general_questions, other_questions } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="faqs_page bg-white p-5 mt-minus">
                <div className="main_content">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-6">
                                <div className="heading_s1 mb-3 mb-md-5">
                                    <h3>{general_questions}</h3>
                                </div>
                                <div id="accordion1" className="accordion accordion_style1">
                                    <div className="card">
                                        <div className="card-header" id="headingSix">
                                            <h6 className="mb-0">
                                                {" "}
                                                <a
                                                    className="collapsed"
                                                    data-toggle="collapse"
                                                    href="#collapseSix"
                                                    aria-expanded="true"
                                                    aria-controls="collapseSix"
                                                >
                                                    What is the weirdest website on the internet?
                    </a>{" "}
                                            </h6>
                                        </div>
                                        <div
                                            id="collapseSix"
                                            className="collapse show"
                                            aria-labelledby="headingSix"
                                            data-parent="#accordion1"
                                        >
                                            <div className="card-body">
                                                <p>
                                                    It is a long established fact that a reader will be
                                                    distracted by the readable content of a page when looking
                                                    at its layout. The point of using Lorem Ipsum is that it
                                                    has a more-or-less normal distribution of letters, as
                                                    opposed to using 'Content here, content here', making it
                                                    look like readable English. Many desktop publishing
                                                    packages and web page editors now use Lorem Ipsum as their
                                                    default model text, and a search for 'lorem ipsum' will
                                                    uncover many web sites still in their infancy.
                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 mt-4 mt-md-0">
                                <div className="heading_s1 mb-3 mb-md-5">
                                    <h3>{other_questions}</h3>
                                </div>
                                <div id="accordion2" className="accordion accordion_style1">
                                    <div className="card">
                                        <div className="card-header" id="headingSix">
                                            <h6 className="mb-0">
                                                {" "}
                                                <a
                                                    className="collapsed"
                                                    data-toggle="collapse"
                                                    href="#collapseSix"
                                                    aria-expanded="true"
                                                    aria-controls="collapseSix"
                                                >
                                                    What is the weirdest website on the internet?
                    </a>{" "}
                                            </h6>
                                        </div>
                                        <div
                                            id="collapseSix"
                                            className="collapse show"
                                            aria-labelledby="headingSix"
                                            data-parent="#accordion2"
                                        >
                                            <div className="card-body">
                                                <p>
                                                    It is a long established fact that a reader will be
                                                    distracted by the readable content of a page when looking
                                                    at its layout. The point of using Lorem Ipsum is that it
                                                    has a more-or-less normal distribution of letters, as
                                                    opposed to using 'Content here, content here', making it
                                                    look like readable English. Many desktop publishing
                                                    packages and web page editors now use Lorem Ipsum as their
                                                    default model text, and a search for 'lorem ipsum' will
                                                    uncover many web sites still in their infancy.
                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
export default FAQS;