import React from "react";
import {BreadcrumbsBar} from "../components/common";
import {ThanksImage,Title,ConfirmMessage,GoHome} from "../components/partials/thanks";

function Thanks() {
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">

        <div className="thank-you mt-minus bg-white">
          <div className="row justify-content-center text-center">
            <div className="col-md-5">
              <ThanksImage />
              <Title />
              <ConfirmMessage />
              <GoHome />
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}

export default Thanks;
