import React from "react";
import { BreadcrumbsBar } from "../components/common";
import { ProductImage, ProductName, ProductTitle, Price, Rating, AddToCart, Description, Color, Size, StockAvailability, Dimensions, Remove } from "../components/partials/compare";

import useTranslation from "../services/use-translation";

function Compare() {
  const { product_image, product_name, price, rating, add_to_cart, description, color, sizes_available, item_availability, in_stock, dimensions, na, weight, remove } = useTranslation();
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">
        <div className="compare_products p-4 mt-minus bg-white">
          <div className="row">
            <div className="col-md-12">
              <div className="compare_box">
                <div className="table-responsive">
                  <table className="table table-bordered text-center">
                    <tbody>
                      <tr className="pr_image">
                        <td className="row_title">{product_image}</td>
                        {
                          [1, 2, 3].map(item => {
                            return (
                              <td className="row_img">
                                <ProductImage />
                              </td>
                            );
                          })
                        }
                      </tr>
                      <tr className="pr_title">
                        <td className="row_title"><ProductTitle text={product_name} /></td>
                        <td className="product_name">
                          <ProductName />
                        </td>
                        <td className="product_name">
                          <ProductName />
                        </td>
                        <td className="product_name">
                          <ProductName />
                        </td>
                      </tr>
                      <tr className="pr_price">
                        <td className="row_title">{price}</td>
                        <td className="product_price">
                          <Price />
                        </td>
                        <td className="product_price">
                          <Price />
                        </td>
                        <td className="product_price">
                          <Price />
                        </td>
                      </tr>
                      <tr className="pr_rating">
                        <td className="row_title">{rating}</td>
                        <td>
                          <Rating />
                        </td>
                        <td>
                          <Rating />
                        </td>
                        <td>
                          <Rating />
                        </td>
                      </tr>
                      <tr className="pr_add_to_cart">
                        <td className="row_title">{add_to_cart}</td>
                        <td className="row_btn">
                          <AddToCart text={add_to_cart} />
                        </td>
                        <td className="row_btn">
                          <AddToCart text={add_to_cart} />
                        </td>
                        <td className="row_btn">
                          <AddToCart text={add_to_cart} />
                        </td>
                      </tr>
                      <tr className="description">
                        <td className="row_title">{description}</td>
                        <td className="row_text">
                          <Description />
                        </td>
                        <td className="row_text">
                          <Description />
                        </td>
                        <td className="row_text">
                          <Description />
                        </td>
                      </tr>
                      <tr className="pr_color">
                        <td className="row_title">{color}</td>
                        <td className="row_color">
                          <Color />
                        </td>
                        <td className="row_color" >
                          <Color />
                        </td>
                        <td className="row_color">
                          <Color />
                        </td>
                      </tr>
                      <tr className="pr_size">
                        <td className="row_title">{sizes_available}</td>
                        <td className="row_size">
                          <Size />
                        </td>
                        <td className="row_size">
                          <Size />
                        </td>
                        <td className="row_size">
                          <Size />
                        </td>
                      </tr>
                      <tr className="pr_stock">
                        <td className="row_title">{item_availability}</td>
                        <td className="row_stock">
                          <StockAvailability text={in_stock} />
                        </td>
                        <td className="row_stock">
                          <StockAvailability text={in_stock} />
                        </td>
                        <td className="row_stock">
                          <StockAvailability text={in_stock} />
                        </td>
                      </tr>
                      <tr className="pr_weight">
                        <td className="row_title">{weight}</td>
                        <td className="row_weight" />
                        <td className="row_weight" />
                        <td className="row_weight" />
                      </tr>
                      <tr className="pr_dimensions">
                        <td className="row_title">{dimensions}</td>
                        <td><Dimensions text={na} /></td>
                        <td><Dimensions text={na} /></td>
                        <td><Dimensions text={na} /></td>
                      </tr>
                      <tr className="pr_remove">
                        <td className="row_title" />
                        {
                          [1, 2, 3].map(product => {
                            return (
                              <td className="row_remove">
                                <Remove text={remove} />
                              </td>
                            )
                          })
                        }
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}

export default Compare;