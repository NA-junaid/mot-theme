import React from "react";
import { BreadcrumbsBar } from "../components/common";
import useTranslation from "../services/use-translation";

function NoResult() {
    const { no_result, result_for, continue_shopping } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="about-us bg-white p-5 mt-minus">
                <div className="row align-items-center">
                    <div className="col-md-12 text-center">
                        <div className="empty-cart">
                            <span className="icon-magnifier" />
                            <h1 className="title mt-3 ">{no_result}</h1>
                            <p>{result_for} 'dddddd'</p>
                            <a href="#" className="mt-3 btn-primary">
                                {continue_shopping}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
}

export default NoResult;