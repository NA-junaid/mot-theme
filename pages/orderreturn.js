import React from "react";
import { BreadcrumbsBar } from "../components/common";
import useTranslation from "../services/use-translation";
import DeliverAddress from "../components/partials/orderreturn/delivery-address";
import PackageDescription from "../components/partials/orderreturn/package-description";

function OrderReturn() {
    const { order_details } = useTranslation();
    return <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
            <div className="order-tracking mt-minus bg-white p-5">
                <h2>{order_details}</h2>
                <DeliverAddress />
                <PackageDescription />

            </div>
        </div>
    </div>
}

export default OrderReturn;