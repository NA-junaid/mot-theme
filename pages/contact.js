import React, { Component } from "react";
import {BreadcrumbsBar} from "../components/common";
import useTranslation from "../services/use-translation";

function Contact(){
  const {address,email,phone} = useTranslation();
    return (
      <div>
        <BreadcrumbsBar />
        <div className="container-fluid">
          {/*================= 
  Start About Us  
  ==================*/}
          <div className="contact_us bg-white p-5 mt-minus">
            <div className="main_content">
              {/* START SECTION CONTACT */}
              <div className="section pb_70">
                <div className="container">
                  <div className="row">
                    <div className="col-xl-4 col-md-6">
                      <div className="contact_wrap contact_style3">
                        <div className="contact_icon">
                          <i className="icon-map" />
                        </div>
                        <div className="contact_text">
                          <span>{address}</span>
                          <p>123 Street, Old , Salmiya, Kuwait</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-4 col-md-6">
                      <div className="contact_wrap contact_style3">
                        <div className="contact_icon">
                          <i className="icon-envelope-open" />
                        </div>
                        <div className="contact_text">
                          <span>{email}</span>
                          <a href="mailto:info@sitename.com">info@yourmail.com </a>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-4 col-md-6">
                      <div className="contact_wrap contact_style3">
                        <div className="contact_icon">
                          <i className="icon-call-out" />
                        </div>
                        <div className="contact_text">
                          <span>{phone}</span>
                          <p>+ 457 789 789 65</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* END SECTION CONTACT */}
              {/* START SECTION CONTACT */}
            </div>
          </div>
        </div>
      </div>
    );
}

export default Contact;