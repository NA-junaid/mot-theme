import React from "react";
import {BreadcrumbsBar} from "../components/common";
import useTranslation from "../services/use-translation";
import {Sidebar,ChangePassword,AddressBook,AddAddress,WishList,MyCancellation} from "../components/partials/accounts";

function Account() {
  const {} = useTranslation();
  return (
    <div>
      <BreadcrumbsBar />
      <div className="container-fluid">
        <div className="my-account white-bg mt-minus">
          <div className="row">
            <Sidebar />
            <div className="col-sm-9">
              <div className="tab-content">
                {/* Tab Edit Acc */}
                <div
                  className="tab-pane fade active show"
                  id="editaccount"
                  role="tabpanel"
                  aria-labelledby="user-communicate-tab"
                >
                  <edit_account />
                </div>
                {/* Tab Change Pass */}
                <div
                  className="tab-pane fade"
                  id="changepass"
                  role="tabpanel"
                  aria-labelledby="user-communicate-tab"
                >
                  <ChangePassword />
                </div>
                {/* Tab Book Address */}
                <div
                  className="tab-pane fade"
                  id="addressbook"
                  role="tabpanel"
                  aria-labelledby="user-users-tab"
                >
                  <AddressBook />
                </div>
                {/* Tab Add Address */}
                <div
                  className="tab-pane fade"
                  id="addaddress"
                  role="tabpanel"
                  aria-labelledby="user-communicate-tab"
                >
                  <AddAddress />
                </div>
                {/* Tab Wish List */}
                <div
                  className="tab-pane fade"
                  id="Wishlist"
                  role="tabpanel"
                  aria-labelledby="user-requests-tab"
                >
                  <WishList />
                </div>
                {/* Tab mycancelation List */}
                <div
                  className="tab-pane fade"
                  id="mycancelation"
                  role="tabpanel"
                  aria-labelledby="user-requests-tab"
                >
                  <MyCancellation /> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Account;
