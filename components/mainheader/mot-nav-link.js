import React from "react";
import Link from "next/link";

function MOTNavLink() {
  return (
    <div className="col-md-2">
      <Link href="/">
        <a className="brand">
          <img src="assets/img/logo.svg" alt="Mall of turkey" />
        </a>
      </Link>
    </div>
  );
}

export default MOTNavLink;
