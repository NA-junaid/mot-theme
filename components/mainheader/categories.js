import React from "react";
import useTranslation from "../../services/use-translation";

function Categories() {
  const {all_Categories} = useTranslation();
  return (
    <div className="col-md-2">
    <div className="dropdown">
    <a
      className="universal_cat"
      href="#"
      role="button"
      id="dropdownMenuLink"
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <i className="icon-menu"></i> {all_Categories}
    </a>

    <div
      className="dropdown-menu cat_dropdown"
      aria-labelledby="dropdownMenuLink"
      x-placement="bottom-start"
      style={{
          position: 'absolute', 
          willChange: 'transform', 
          top: '0px',
          left: '0px', 
          transform: 'translate3d(0px, 54px, 0px)'
      }}
    >
      <ul>
        <li>
          <a href="#">Home &amp; Garden</a>
        </li>
        <li>
          <a href="#">Jewellery &amp; Watches</a>
        </li>
        <li>
          <a href="#">Phones &amp; Accessories</a>
        </li>
        <li>
          <a href="#">Consumer Electronics</a>
        </li>
        <li>
          <a href="#">Tools &amp; Home</a>
        </li>
        <li>
          <a href="#">Automobiles</a>
        </li>
        <li>
          <a href="#">Beauty &amp; Health</a>
        </li>
        <li>
          <a href="#">Baby &amp; Kids</a>
        </li>
        <li>
          <a href="#">Sports &amp; Entertainment</a>
        </li>
        <li>
          <a href="#">Women's Clothing</a>
        </li>
        <li>
          <a href="#">Toys &amp; Hobbies</a>
        </li>
        <li>
          <a href="#">Computer &amp; Office</a>
        </li>
        <li>
          <a href="#">Men's Clothing</a>
        </li>
      </ul>
    </div>
  </div>
  </div>
  );
}

export default Categories;
