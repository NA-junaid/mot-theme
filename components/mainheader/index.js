import React from "react";

import MOTNavLink from './mot-nav-link';
import Categories from './categories';
import SearchArea from './search-area';
import SellOnMOT from './sell-on-mot';

function MainHeader() {
  return (
    <header className="pt-4">
      <div className="container-fluid">
        <div className="row">
          <MOTNavLink />
          <Categories />
          <SearchArea />
          <SellOnMOT/>
        </div>
      </div>
    </header>
  );
}
export default MainHeader;
