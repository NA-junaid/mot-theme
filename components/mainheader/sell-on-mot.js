import React from "react";
import Link from "next/link";
import useTranslation from "../../services/use-translation";

function SellOnMOT() {
    const { sell_your , products } = useTranslation();
    return <div className="col-md-3">
        <div className="cart_block mt-3 text-right">
            <ul>
                <li>
                    <a href="#">
                        <i className="icon-layers text-secondary"></i>{" "}
                        <span>
                            <small>{sell_your}</small> <br />
                            {products}
                        </span>
                    </a>
                </li>
                <li>
                    <Link href="cart">
                        <a href="#">
                            <i className="icon-basket"></i>
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="">
                    <a href="#">
                        <i className="icon-heart"></i>
                    </a>
                    </Link>
                </li>
            </ul>
        </div>
    </div>
}


export default SellOnMOT;
