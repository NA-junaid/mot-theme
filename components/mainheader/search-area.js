import React from "react";
import useTranslation from "../../services/use-translation";

function SearchArea() {
    const {type_Here} = useTranslation();
    return (
        <div className="col-md-5">
            <div className="search_area">
                <input type="text" placeholder={type_Here} name="" />
                <button className="search_btn bg-secondary" type="submit">
                    <i className="icon-magnifier"></i>
                </button>
            </div>
        </div>
    );
}

export default SearchArea;
