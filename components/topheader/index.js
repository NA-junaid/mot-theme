import React from "react";
import Language from './language';
import Currency from './currency';
import ShipTo from './ship-to';
import HelpCenter from './help-center';
import SellOn from './sell-on';
import User from './user';

function TopHeader() {
  return (
    <div className="top_header p-2">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="d-flex justify-content-between">
              <HelpCenter />
              <div className="account_setting">
                <div className="d-flex justify-content-between">
                  <Language />
                  <Currency />
                  <SellOn />
                  <User />
                  <ShipTo />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TopHeader;

