import React from "react";
import useTranslation from "../../services/use-translation";

function ShipTo() {
    const {ship_to} = useTranslation();
    return (
        <div className="country">
            <div className="dropdown pl-2">
                <button
                    className="dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    {ship_to}
                      </button>
                <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                >
                    <a className="dropdown-item" href="#">
                        Kuwait
                        </a>
                    <a className="dropdown-item" href="#">
                        Saudi
                        </a>
                    <a className="dropdown-item" href="#">
                        Turkey
                        </a>
                </div>
            </div>
        </div>
    );
}

export default ShipTo;
