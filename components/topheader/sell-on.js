import React from "react";
import useTranslation from "../../services/use-translation";

function SellOn() {
  const {sell_on_mot} = useTranslation();
  return (
    <div className="sell_on  pl-3 pr-3  border-right">
                    <a href="#" className="text-secondary">
                      {sell_on_mot}
                    </a>
                  </div>
  );
}

export default SellOn;
