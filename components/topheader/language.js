import React, { useEffect } from "react";
import useTranslation from "../../services/use-translation";
import { useDispatch, useSelector } from "react-redux";

function Language() {
    const { languages } = useTranslation();

    const langs = useSelector((state) => state.languages);
    console.log(langs);
    const dispatch = useDispatch();
    const filtered = langs.filter((item) => item.isDefault);
    const currentLanguage = filtered[0];
    const handleChange = (l) => {
        const lang = JSON.parse(l);
        if (currentLanguage.short_name != lang.short_name) {
            dispatch(setDefault(lang));
        }
    };
    useEffect(() => {}, [languages]);
    return (
        <div className="dropdown border-right">
            <button
                className="dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
            >
                {languages}
            </button>
            <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
            >
                {langs.map(language => {
                    return (
                        <a className="dropdown-item"
                        href="#"
                        key={language.short_name}
                        onClick={() => handleChange(JSON.stringify(language))}
                        >
                            {language.symbol}
                        </a>
                    )
                })}
                <a className="dropdown-item" href="#">
                    {currentLanguage.symbol}
                </a>
            </div>
        </div>
    );
}

export default Language;
