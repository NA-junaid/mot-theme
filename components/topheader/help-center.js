import React from "react";
import useTranslation from "../../services/use-translation";

function HelpCenter() {
    const {help_Center , or , email_Us} = useTranslation();
    return (
    <span className="phone">
            {help_Center}: +968572458 {or} {email_Us}: sales @mot.com
    </span>
    );

}

export default HelpCenter;