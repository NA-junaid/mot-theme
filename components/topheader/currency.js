import React from "react";
import useTranslation from "../../services/use-translation";

function Currency() {
    const { currency } = useTranslation();
    return (

        <div className="dropdown pl-2 pr-2  border-right">
            <button
                className="dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
            >
                {currency}
            </button>
            <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
            >
                <a className="dropdown-item" href="#">
                    KWD
  </a>
                <a className="dropdown-item" href="#">
                    USD
  </a>
                <a className="dropdown-item" href="#">
                    AUD
  </a>
            </div>
        </div>
    );
}

export default Currency;

