import React, { useState } from "react";
import useLanguage from "../../../services/use-language";

function Slider({ banners }) {
  const lang = useLanguage();
  const [bannerID,setBannerID] = useState(banners[0].id);
  return (
    <div className="col-md-9">
      <div className="main_slider">
        <div id="demo" className="carousel slide" data-ride="carousel">
          <div className="carousel-inner">
            {banners.map(banner => {
              let activeClass = banner.id==bannerID?"active":"";
              return (
                <div className={`carousel-item ${activeClass}`} key={banner.id}>
                  <img
                    src={banner.url}
                    className="Watches"
                  />
                </div>
              )
            })}
          </div>
          <ul className="carousel-indicators">
            {
              banners.map(banner => {
                return (
                  <div>
                    <li data-target="#demo" data-slide-to={0}  key={banner.id} onClick={()=>setBannerID(banner.id)}>
                      <img src={banner.url} alt={banner[lang]} />
                    </li>
                  </div>
                )
              })
            }
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Slider;
