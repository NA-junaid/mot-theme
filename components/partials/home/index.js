import Banner from './banner';
import CategorySidebar from './category-side-bar';
import Deals from './deals';
import DeliveryProcess from './delivery-process';
import FeaturedCategory from './featured-category';
import FullAdvertisement from './full-advertisement';
import ProductList from './product-list';
import ProductSection from './product-section';
import ProductView from './product-view';
import ProductsGrid from './products-grid';
import Slider from './slider';
import SmallProduct from './small-product';
import ThreeColumnAdvertisement from './three-column-advertisement';
import TodayDeals from './today-deals';
import TopLinks from './top-links';
import TrendingProducts from './trending-products';

export {
    Banner,
    CategorySidebar,
    Deals,
    DeliveryProcess,
    FeaturedCategory,
    FullAdvertisement,
    ProductList,
    ProductSection,
    ProductView,
    ProductsGrid,
    Slider,
    SmallProduct,
    ThreeColumnAdvertisement,
    TodayDeals,
    TopLinks,
    TrendingProducts
};