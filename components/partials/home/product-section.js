import React from "react";
import ProductsGrid from './products-grid';
import ProductList from './product-list';

function ProductSection() {
    return (
        <div className="fashion_products">
            <div className="container-fluid">
            <ProductList />
                <div className="row">
               
                    {/* Tabs Area Star*/}
                    <div className="col-md-12">
                        <div className="tabs_container">
                            
                            <ProductsGrid />
                        </div>
                    </div>
                    {/* tabs Area Ends */}
                </div>
            </div>
        </div>
    );
}

export default ProductSection;


