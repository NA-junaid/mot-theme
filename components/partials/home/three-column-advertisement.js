import React from "react";
import useTranslation from "../../../services/use-translation";

function ThreeColumnAdvertisement() {
  const { sell_your_products, affordable_price, sell_now } = useTranslation();
  return (
    <div className="container-fluid three_col">
      <div className="row">
        {
          [1, 2, 3].map(no => {
            return (
              <div className="col-md-4">
                <div className="col1 sell_products">
                  <h3>{sell_your_products}</h3>
                  <h4>{affordable_price}</h4>
                  <a href="#">
                    {sell_now} <i className="icon-arrow-right" />
                  </a>
                </div>
              </div>
            );
          })
        }
      </div>
    </div>
  );
}

export default ThreeColumnAdvertisement;
