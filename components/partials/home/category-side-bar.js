import React from "react";
import Link from 'next/link';
import useTranslation from "../../../services/use-translation";

function CategorySidebar() {
  const { all_categories, home, view_all, } = useTranslation();
  return (
    <div className="col-md-3">
      <div className="categories">
        <h1 className="p-3">
          <i className="icon-menu" /> {all_categories}
        </h1>
        <ul>
          <li>
            <Link href="/products">
              <a>{home} &amp; Garden</a>
            </Link>
          </li>
        </ul>
        <a href="#" className="viewall_cat">
          {view_all}
        </a>
      </div>
    </div>
  );
}

export default CategorySidebar;
