import React from "react";
import ProductView from './product-view';

function ProductsGrid() {
    return (
        <div className="tab-content" >
            <div
                className="tab-pane active"
                id="home"
                role="tabpanel"
            >
                {/* Products Start here */}
                <div className="row">
                    <div className="row products_container">
                        {
                            [1, 2, 3, 4, 5, 6].map((i) => {
                                return <ProductView key={i}/>
                            })
                        }
                    </div>
                </div>
                {/* Products Ends here */}
            </div>

            {/* Products Ends here */}
        </div>


    );
}

export default ProductsGrid;
