import React from "react";
import useTranslation from "../../../services/use-translation";

function DeliveryProcess() {
  const {turkish_products,find_products,secure_payment,protection_system,easy_return,reliable_return,allday,support,ready_to_help_you} =  useTranslation();
  return (
    <div className="delivery_process">
      <span>
        <i className="icon-layers"></i>
        <h4>
          {turkish_products}<small>{find_products}</small>
        </h4>
      </span>
      <span>
        <i className="icon-wallet"></i>
        <h4>
          {secure_payment} <small>{protection_system}</small>
        </h4>
      </span>
      <span>
        <i className="icon-action-redo"></i>
        <h4>
          {easy_return}<small>{reliable_return}</small>
        </h4>
      </span>
      <span>
        <i className="icon-call-in"></i>
        <h4>
          {`${allday} ${support}`}<small>{ready_to_help_you}</small>
        </h4>
      </span>
    </div>
  );
}

export default DeliveryProcess;
