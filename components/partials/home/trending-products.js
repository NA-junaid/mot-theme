import React from "react";
import useTranslation from "../../../services/use-translation";

function TrendingProducts() {
  const {trending_products} = useTranslation();
  return (
    <div className="col-md-12">
    <h2 className=" float-left">{trending_products}</h2>
    <ul className="cat_list">
      <li>
        <a href="#" className="active">
          ALL
          </a>
      </li>
      <li>
        <a href="#">TV</a>
      </li>
      <li>
        <a href="#">COMPUTERS</a>
      </li>
      <li>
        <a href="#">TABLETS &amp; CELL PHONES</a>
      </li>
      <li>
        <a href="#">SMARTWATCHES</a>
      </li>
      <li>
        <a href="#">ACCESSORIES</a>
      </li>
    </ul>
  </div>
  );
}

export default TrendingProducts;
