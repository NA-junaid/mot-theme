import React from "react";
import TrendingProducts from './trending-products';

function ProductList() {
  return (
    <div className="trending_products mt-4 pt-2 mb-2">
<div className="container-fluid">
  <div className="row">
    <TrendingProducts/>
    <div className="col-md-12">
      <div id="owl-carousel" className="owl-carousel owl-theme">

      {
            [1, 2, 3, 4, 5].map(product => {
              return (
                <div className="item">
                <div className="trend_pro_box">
                  <span className="new_offer">New</span>
                  <span className="top_offer">Top</span>
                  <span className="sale_offer">Sale</span>
                  <a className="wishist">
                    <i className="icon-heart" />
                  </a>
                  <img src="assets/img/home_products/trepro1.png" />
                  <h4>Sound Port</h4>
                  <h3>Electro Store</h3>
                  <div className="">
                    <span>KD 99.84</span>
                    <span className="offer_price">KD2587</span>
                  </div>
                  <div className="cart_footer">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="icon-basket" />
                        </a>
                      </li>
                      <li>
                        <a href="#" className="bordernone">
                          <i className="icon-eye" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              )
            })
          }

      
      
      </div>
    </div>
  </div>
</div>
</div>
  );
}

export default ProductList;

