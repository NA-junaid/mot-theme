import React from "react";
import useTranslation from "../../../services/use-translation";

function TodayDeals() {
  const { ends_in, todays_deals, per_off } = useTranslation();
  return (
    <div className="today_deals ">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <h2 className="mb-3">{todays_deals}</h2>
          </div>
          {
            [1, 2, 3, 4].map(product => {
              return (
                <div key={product} className="col-md-3">
                  <span className="timer">{ends_in} 06:56:37</span>
                  <div className="offers">
                    <h3>25{per_off}</h3>
                    <h4>Sneakers</h4>
                    <h5>KWD 250</h5>
                  </div>
                  <img src="assets/img/home_products/deal1.png" alt="Deals" />
                </div>
              )
            })
          }
        </div>
      </div>
    </div>
  );
}

export default TodayDeals;
