import React from "react";

function ProductView() {
  return (
    <div className="col-md-6">
    <img
      src="assets/img/home_products/tr01.png"
      className="float-left"
    />
    <div className="pp_item">
      <h2>Fashion products Allen Solly Tan Handbag</h2>
      <span className="vendors">Fasstrack</span>
      <div className="price">
        <span>KD 99.84</span>
        <span className="offer_price">KD2587</span>
      </div>
      <span className="add_cart">
        <i className="icon-basket" />
      </span>
    </div>
  </div>
  );
}

export default ProductView;
