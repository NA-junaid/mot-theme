import React from "react";
import useTranslation from "../../../services/use-translation";

function TopLinks() {
  const {flash_deals,daily_deals, new_arrivals, inspection_goods, just_for_you, made_in_turkey} = useTranslation();
  return (
    <ul className="list_items_block">
      <li>
        <a href="#" className="active">
          {flash_deals}
        </a>
      </li>
      <li>
        <a href="#">{daily_deals}</a>
      </li>
      <li>
        <a href="#">{new_arrivals}</a>
      </li>
      <li>
        <a href="#">{inspection_goods}</a>
      </li>
      <li>
        <a href="#">{just_for_you}</a>
      </li>
      <li>
        <a href="#">{made_in_turkey}</a>
      </li>
    </ul>
  );
}

export default TopLinks;
