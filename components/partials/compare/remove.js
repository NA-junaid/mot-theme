import React from "react";

function Remove(props) {
    return (
        <div>
            <a href="#">
                <span>{props.text}</span> <i className="fa fa-times" />
            </a>
        </div>
    );
}

export default Remove;
