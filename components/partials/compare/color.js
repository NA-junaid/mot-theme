import React from "react";

function Color() {
    return (
        <div className="product_color_switch">
            <span
                data-color="#87554B"
                style={{ backgroundColor: "rgb(135, 85, 75)" }}
            />
            <span
                data-color="#333333"
                style={{ backgroundColor: "rgb(51, 51, 51)" }}
            />
            <span
                data-color="#DA323F"
                style={{ backgroundColor: "rgb(218, 50, 63)" }}
            />
        </div>
    );
}

export default Color;
