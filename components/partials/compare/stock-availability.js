import React from "react";

function StockAvailability(props) {
  return (
  <div>
    <span className="in-stock">{props.text}</span>
  </div>
  );
}

export default StockAvailability;
