import React from "react";

function Price() {
    return (
        <div>
            <span className="price">
                <ccc
                    title="$45.00"
                    className="ccc--converted"
                    style={{
                        fontSize: "inherit",
                        display: "inline",
                        color: "inherit"
                    }}
                >
                    $70.88
                      </ccc>
            </span>
        </div>
    );
}

export default Price;
