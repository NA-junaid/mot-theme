import React from "react";

function AddToCart(props) {
    return (
        <div>
            <a href="#" className="btn btn-fill-out">
                <i className="icon-basket-loaded" /> {props.text}
                    </a>
        </div>
    );
}

export default AddToCart;
