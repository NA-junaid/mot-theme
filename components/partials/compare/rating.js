import React from "react";

function Rating() {
    return (
        <div>
            <div className="rating_wrap">
                <div className="rating">
                    <div
                        className="product_rate"
                        style={{ width: "80%" }}
                    />
                </div>
                <span className="rating_num">(21)</span>
            </div>
        </div>
    );
}

export default Rating;
