import React from "react";
import SearchArea from "./search-area";
import Image from "./image";

function ProductImage() {
    return (
        <div>
            <SearchArea />
            <Image />
        </div>
    );
}

export default ProductImage;
