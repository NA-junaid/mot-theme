import AddToCart from './add-to-cart';
import Color from './color';
import Description from './description';
import Dimensions from './dimensions';
import Image from './image';
import Price from './price';
import ProductImage from './product-image';
import ProductName from './product-name';
import ProductTitle from './product-title';
import Rating from './rating';
import Remove from './remove';
import SearchArea from './search-area';
import Size from './size';
import StockAvailability from './stock-availability';

export {AddToCart,Color,Description,Dimensions,Image,Price,ProductImage,ProductName,ProductTitle,Rating,Remove,SearchArea,Size,StockAvailability};