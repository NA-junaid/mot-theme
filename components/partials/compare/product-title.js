import React from "react";

function ProductTitle(props) {
  return (
  <div>
    <div className="row_title">{props.text}</div>
  </div>
  );
}

export default ProductTitle;
