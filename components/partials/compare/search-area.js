import React from "react";
import useTranslation from "../../../services/use-translation";

function SearchArea() {
    const {search_compare_with,search} = useTranslation();
    return (
        <div>
            <form className="form-inline">
                <div className="form-group mx-sm-3 mb-2">
                    <input
                        type="text"
                        className="form-control"
                        id="input1"
                        placeholder={search_compare_with}
                    />
                </div>
                <button
                    type="submit"
                    className=" d-inline btn-primary mb-2"
                >
                    {search}
                        </button>
            </form>
        </div>
    );
}

export default SearchArea;
