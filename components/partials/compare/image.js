import React from "react";

function Image() {
    return (
        <div>
            <img
                src="assets/images/product_img1.jpg"
                alt="compare-img"
            />
        </div>
    );
}

export default Image;
