import React from "react";
import useTranslation from "../../../services/use-translation";

function HistoryItems() {
    const { delivered , no_return_policy , rate_review} = useTranslation();
    return (
        <div className="table-responsive">
            <table className="table">
                <tbody>
                    <tr>
                        <th scope="row" className="border-0">
                            <div className="p-2">
                                <img
                                    src="assets/img/ord1.jpeg"
                                    alt
                                    width={70}
                                    className="img-fluid rounded shadow-sm"
                                />
                                <div className="ml-3 d-inline-block align-middle">
                                    <h5 className="mb-0">
                                        {" "}
                                        <a
                                            href="#"
                                            className="text-dark d-inline-block align-middle"
                                        >
                                            Abbzorb Nutrition L-Glutamine Glutamine
                      </a>
                                    </h5>
                                    <span className="text-muted font-weight-normal  d-block">
                                        <strong>Seller: </strong> Abbzorb Nutrition
                    </span>
                                </div>
                            </div>
                        </th>
                        <td className="border-0 align-middle" width={320}>
                            <span className="font-weight-normal  d-block">
                                <b>KD 249.00</b>{" "}
                            </span>
                        </td>
                        <td className="border-0 align-middle" width={320}>
                            <h5 className="mb-1">
                                <span className="active_green" />
                                {delivered} 13-Sep-2019
                </h5>
    <small>{no_return_policy}</small>
                            <a
                                href="#"
                                className="btn-primary btn-block mt-2 text-center"
                            >
                               {rate_review}
                </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr />
        </div>

    );
}

export default HistoryItems;
