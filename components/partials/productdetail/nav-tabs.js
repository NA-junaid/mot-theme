import React from "react";
import useTranslation from "../../../services/use-translation";

function NavTabs() {
    const {description,additional_info,reviews} = useTranslation();
    return (
        <div>
            <ul className="nav nav-tabs" role="tablist">
                <li className="nav-item">
    <a className="nav-link " id="Description-tab" data-toggle="tab" href="#Description" role="tab" aria-controls="Description" aria-selected="true">{description}</a>
                </li>
                <li className="nav-item">
    <a className="nav-link active" id="Additional-info-tab" data-toggle="tab" href="#Additional-info" role="tab" aria-controls="Additional-info" aria-selected="false">{additional_info}</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link " id="Reviews-tab" data-toggle="tab" href="#Reviews" role="tab" aria-controls="Reviews" aria-selected="false">{reviews} (2)</a>
                </li>
            </ul>
        </div>
    );
}

export default NavTabs;
