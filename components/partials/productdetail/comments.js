import React from "react";
import useTranslation from "../../../services/use-translation";

function Comments() {
    const {reviews_for} = useTranslation();
    return (
        <div className="comments">
            <h5 className="product_tab_title">2 {reviews_for} <span>Blue Dress For Woman</span></h5>
            <ul className="list_none comment_list mt-4">
                <li>
                    <div className="comment_img">
                        <img src="assets/images/user1.jpg" alt="user1" />
                    </div>
                    <div className="comment_block">
                        <div className="rating_wrap">
                            <div className="rating">
                                <div className="product_rate" style={{ width: '80%' }} />
                            </div>
                        </div>
                        <p className="customer_meta">
                            <span className="review_author">Alea Brooks</span>
                            <span className="comment-date">March 5, 2018</span>
                        </p>
                        <div className="description">
                            <p>Lorem Ipsumin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="comment_img">
                        <img src="assets/images/user2.jpg" alt="user2" />
                    </div>
                    <div className="comment_block">
                        <div className="rating_wrap">
                            <div className="rating">
                                <div className="product_rate" style={{ width: '60%' }} />
                            </div>
                        </div>
                        <p className="customer_meta">
                            <span className="review_author">Grace Wong</span>
                            <span className="comment-date">June 17, 2018</span>
                        </p>
                        <div className="description">
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    );
}

export default Comments;
