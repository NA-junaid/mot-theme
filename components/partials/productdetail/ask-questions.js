import React from "react";

function AskQuestions({text}) {
  return (
  <div>
     <button className="btn btn-block btn-primary mt-3 mb-3">{text}</button>
  </div>
  );
}

export default AskQuestions;
