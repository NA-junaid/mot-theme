import Actions from './actions';
import AdditionalInfo from './additional-info';
import AddYourReview from './add-your-review';
import Colors from './colors';
import Comments from './comments';
import ImagePreview from './image-preview';
import MemberArea from './member-area';
import MemberPicture from './member-picture';
import MemberSince from './member-since';
import NavTabs from './nav-tabs';
import OtherProducts from './other-products';
import Price from './price';
import ProductDescription from './product-description';
import ProductDetail from './product-detail';
import ProductInfo from './product-info';
import ProductTitle from './product-title';
import Quantity from './quantity';
import Rating from './rating';
import ShopInfo from './shop-info';
import Sizes from './sizes';
import Thumbnail from './thumb-nail';
import ViewProfile from './view-profile';
import Vote from './vote';

export {
    Actions,
    AdditionalInfo,
    AddYourReview,
    Colors,
    Comments,
    ImagePreview,
    MemberArea,
    MemberPicture,
    MemberSince,
    NavTabs,
    OtherProducts,
    Price,
    ProductDescription,
    ProductDetail,
    ProductInfo,
    ProductTitle,
    Quantity,
    Rating,
    ShopInfo,
    Sizes,
    Thumbnail,
    ViewProfile,
    Vote

};