import React from "react";

function ViewProfile({text}) {
    return (
        <div className="vprofile">
            Online Shoes Store <a href="#" className="view_profile">{text}</a>
        </div>
    );
}

export default ViewProfile;
