import React from "react";

function Colors({text}) {
    return (
        <div>
            <h5 className="colors">{text}:
              <span className="color orange not-available" data-toggle="tooltip" title="Not In store" />
                <span className="color green" />
                <span className="color blue" />
            </h5>
        </div>
    );
}

export default Colors;
