import React from "react";
import ProductTitle from "./product-title";
import Rating from "./rating";
import ViewProfile from "./view-profile";
import ProductDescription from "./product-description";
import Price from "./price";
import Vote from "./vote";
import Sizes from "./sizes";
import Colors from "./colors";
import Quantity from "./quantity";
import Actions from "./actions";
import useTranslation from "../../../services/use-translation";

function ProductInfo() {
    const {reviews,view_profile,votes,sizes,colors,add_to_cart,compare,add_to_wishlist} = useTranslation();
    return (

        <div className="details col-md-4">
            <ProductTitle />
            <Rating text={reviews}/>
            <ViewProfile text={view_profile}/>
            <ProductDescription />
            <Price />
            <Vote text={votes}/>
            <Sizes text={sizes}/>
            <Colors text={colors}/>
            <Quantity text={add_to_cart}/>
            <Actions compare={compare} add_to_wishlist={add_to_wishlist}/>
        </div>

    );
}

export default ProductInfo;
