import React from "react";

function Quantity({text}) {
    return (
        <div className="range_nunber d-flex justify-content-start">
            <input type="number" defaultValue={1} min={0} max={100} step={1} />
    <button className=" addtocart_d" type="button"><i className="icon-handbag" /> {text}</button>
        </div>
    );
}

export default Quantity;
