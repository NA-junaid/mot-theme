import React from "react";
import ImagePreview from "./image-preview";
import Thumbnail from "./thumb-nail";
import ProductInfo from "./product-info";
import MemberArea from "./member-area";

function ProductDetail(props) {
    return (
        <div className="bg-white p-4">
            <div className="row no-gutters">
                <div className="col-md-12">
                    <div className="wrapper_details row">
                        <div className="preview col-md-5">
                            <ImagePreview />
                            <Thumbnail />
                        </div>
                        <ProductInfo />
                        <MemberArea />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductDetail;
