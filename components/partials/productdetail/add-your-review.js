import React from "react";
import useTranslation from "../../../services/use-translation";

function AddYourReview() {
    const { add_you_review, comments, name, email,submit_review,message, upload_photo } = useTranslation();
    return (
        <div className="review_form field_form">
            <h5>{add_you_review}</h5>
            <form className="row mt-3">
                <div className="form-group col-12">
                    <div className="star_rating">
                        <span data-value={1}><i className="fa fa-star" /></span>
                        <span data-value={2}><i className="fa fa-star" /></span>
                        <span data-value={3}><i className="fa fa-star" /></span>
                        <span data-value={4}><i className="fa fa-star" /></span>
                        <span data-value={5}><i className="fa fa-star" /></span>
                    </div>
                </div>
                <div className="form-group col-12">
                    <label>{comments}</label>
                    <textarea required="required" placeholder="Your review " className="form-control" name={message} rows={4} defaultValue={""} />
                </div>
                <div className="form-group col-md-6">
                    <label>{name}</label>
                    <input required="required" placeholder="Enter Name *" className="form-control" name={name} type="text" />
                </div>
                <div className="form-group col-md-6">
                    <label>{email}</label>
                    <input required="required" placeholder="Enter Email *" className="form-control" name={email} type="email" />
                </div>
                <div className="form-group col-md-6">
                    <label>{upload_photo}</label>
                    <input required="required" className="form-control" type="file" placeholder={upload_photo} />
                </div>
                <div className="form-group col-12">
                    <button type="submit" className="btn btn-fill-out" name="submit" value="Submit">{submit_review}</button>
                </div>
            </form>
        </div>
    );
}

export default AddYourReview;
