import React from "react";

function OtherProducts({ other_products, view_list }) {
    return (
        <div>
            <h2>{other_products}:</h2>
            <div className="mm_products">
                <div className="row">
                    <div className="col-md-6">
                        <figure><img src="assets/img/products/img1.jpg" className="img-fluid" /></figure>
                    </div>
                    <div className="col-md-6">
                        <h4 className="mm_title">Jordan Galaxy</h4>
                        <h5 className="mm_price mt-2">KD 280</h5>
                    </div>
                </div>
            </div>
            <button className="btn btn-block btn-primary mt-3 mb-3">{view_list}</button>
        </div>
    );
}

export default OtherProducts;
