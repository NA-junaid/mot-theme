import React from "react";

function Sizes({ text }) {
    return (
        <div>
            <h5 className="sizes">{text}:
              <span className="size active" data-toggle="tooltip" title="small">35</span>
                <span className="size" data-toggle="tooltip" title="medium">40</span>
                <span className="size" data-toggle="tooltip" title="large">42</span>
                <span className="size" data-toggle="tooltip" title="xtra large">42.2</span>
            </h5>
        </div>
    );
}

export default Sizes;
