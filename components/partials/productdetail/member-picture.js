import React from "react";

function MemberPicture() {
    return (
        <div className="mm_photo text-center">
            <span><img src="assets/img/avatar.png" /></span>
            <h2>Fashion Store</h2>
            <hr />
        </div>
    );
}

export default MemberPicture;
