import React from "react";

function Thumbnail() {
    return (
        <div>
            <ul className="preview-thumbnail nav nav-tabs">
                {
                    [1, 2, 3, 4, 5].map(product => {
                        return (
                            <li className="active"><a data-target="#pic-1" data-toggle="tab"><img src="assets/img/products-detail/img2.jpg" /></a></li>
                        )
                    })
                }
            </ul>
        </div>
    );
}

export default Thumbnail;
