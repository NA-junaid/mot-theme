import React from "react";

function AdditionalInfo() {
    return (
        <div className="tab-pane fade" id="Additional-info" role="tabpanel" aria-labelledby="Additional-info-tab">
            <table className="table table-bordered">
                <tbody>
                    <tr>
                        <td>Capacity</td>
                        <td>5 Kg</td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td>Black, Brown, Red,</td>
                    </tr>
                    <tr>
                        <td>Water Resistant</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Material</td>
                        <td>Artificial Leather</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default AdditionalInfo;
