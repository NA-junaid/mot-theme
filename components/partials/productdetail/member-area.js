import React from "react";
import MemberPicture from "./member-picture";
import MemberSince from "./member-since";
import AskQuestions from "./ask-questions";
import OtherProducts from "./other-products";
import useTranslation from "../../../services/use-translation";

function MemberArea() {
    const {member_since,ask_to_seller,other_products,view_list} = useTranslation();
    return (
        <div className="col-md-3">
            <div className="member_area">
                <MemberPicture />
                <MemberSince text={member_since}/>
                <AskQuestions text={ask_to_seller}/>
                <OtherProducts other_products={other_products} view_list={view_list}/>
            </div>
        </div>
    );
}

export default MemberArea;
