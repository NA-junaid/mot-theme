import React from "react";

function Vote({text}) {
  return (
  <div>
    <p className="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 {text})</strong></p>
  </div>
  );
}

export default Vote;
