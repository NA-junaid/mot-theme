import React from "react";

function Actions({ compare, add_to_wishlist }) {
    return (
        <div className="action">
            <button className="add-to-cart " type="button"><span className="icon-shuffle" /> {compare}</button>
            <button className="like " type="button"><span className="icon-heart" /> {add_to_wishlist}</button>
        </div>
    );
}

export default Actions;
