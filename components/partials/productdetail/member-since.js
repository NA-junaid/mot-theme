import React from "react";

function MemberSince({ member_since }) {
    return (
        <div className="d-flex justify-content-between">
            <span>{member_since}:</span>
            <span className="primary-color">8 / 8 / 2008</span>
        </div>

    );
}

export default MemberSince;
