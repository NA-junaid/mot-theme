import React from "react";

function Rating({ text }) {
  return (
    <div className="rating">
      <div className="stars">
        <span className="fa fa-star checked" />
        <span className="fa fa-star checked" />
        <span className="fa fa-star checked" />
        <span className="fa fa-star" />
        <span className="fa fa-star" />
      </div>
      <span className="review-no">41 {text}</span>
    </div>
  );
}

export default Rating;
