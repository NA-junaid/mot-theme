import React from "react";

function DeliveryAddress() {
  return (
    <div>
      <form action>
        <ul>
          <li>
            <div className="custom-control custom-radio custom-control-inline">
              <input
                type="radio"
                className="custom-control-input"
                id="customRadio5"
                name="example"
              />
              <label
                className="custom-control-label"
                htmlFor="customRadio5"
              >
                Chand Khan
          </label>
            </div>
            <span className="bg-secondary p-1 pr-2 pl- text-white rounded ml-2">
              Home
        </span>
            <p className="mt-2">
              120, Main Street Block A Flat no 20, Salmiya, Kuwait{" "}
            </p>
          </li>
          <li>
            <div className="custom-control custom-radio custom-control-inline">
              <input
                type="radio"
                className="custom-control-input"
                id="customRadio6"
                name="example"
              />
              <label
                className="custom-control-label"
                htmlFor="customRadio6"
              >
                Chand khan2
          </label>
            </div>
            <span className="bg-secondary p-1 pr-2 pl-2 text-white rounded ml-2">
              Office
        </span>
            <p className="mt-2">
              220, Main Street Block D Flat no 30, Salmiya, Kuwait{" "}
            </p>
          </li>
        </ul>
      </form>
    </div>
  );
}

export default DeliveryAddress;
