import React from "react";
import useTranslation from "../../../services/use-translation";

function PayNow() {
    const {pay_now} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-12">
                <button
                    type="submit"
                    className="btn btn-primary delivery-here"
                >
                    {pay_now}
</button>
            </div>
        </div>
    );
}

export default PayNow;
