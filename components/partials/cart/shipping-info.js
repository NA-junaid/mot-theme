import React from "react";
import DeliveryAddress from "./delivery-address";
import NameField from "./name-field";
import PinCode from "./pin-code";
import Address from "./address";
import City from "./city";
import LandMark from "./land-mark";
import RadioButton from "./radio-button";
import DeliveryHere from "./delivery-here";
import Login from "./login";
import SignUp from "./sign-up";
import Coupons from "./coupons";


function ShippingInfo() {
    return (
        <div className="row">
            <div className="col-md-8">
                <div className="delivery-address">
                    <DeliveryAddress />
                    <form>
                        <NameField />
                        <PinCode />
                        <Address />
                        <City />
                        <LandMark />
                        <RadioButton />
                        <DeliveryHere />
                        <div className="form-row">
                            <Login />
                            <SignUp />
                        </div>
                    </form>
                </div>
            </div>
            <Coupons />
        </div>
    );
}

export default ShippingInfo;
