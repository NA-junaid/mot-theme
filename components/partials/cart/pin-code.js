import React from "react";
import useTranslation from "../../../services/use-translation";

function PinCode() {
    const {pincode , locality} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={pincode}
                />
            </div>
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={locality}
                />
            </div>
        </div>
    );
}

export default PinCode;
