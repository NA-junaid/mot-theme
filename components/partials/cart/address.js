import React from "react";
import useTranslation from "../../../services/use-translation";

function Address() {
    const  {address_area_street} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-12">
                <textarea
                    className="form-control"
                    placeholder= {address_area_street}
                    defaultValue={""}
                />
            </div>
        </div>
    );
}

export default Address;
