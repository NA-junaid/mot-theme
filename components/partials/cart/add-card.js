import React from "react";
import useTranslation from "../../../services/use-translation";

function AddCard() {
    const {add_debit_credit_atm} = useTranslation();
   
    
    return (
        <div className="custom-control custom-radio custom-control-inline">
            <input
                type="radio"
                className="custom-control-input"
                id="customRadio9"
                name="example"
            />
            <label
                className="custom-control-label"
                htmlFor="customRadio9"
            >
               {add_debit_credit_atm}
</label>
        </div>
    );
}

export default AddCard;
