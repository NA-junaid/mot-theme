import React from "react";
import useTranslation from "../../../services/use-translation";

function SignUp() {
    const {if_already_user_please , singup} = useTranslation();
    return (
        <div className="form-group col-md-6">
            <a href="#." className="btn btn-login">
                {if_already_user_please} <span>{singup}</span>
            </a>
        </div>
    );
}

export default SignUp;
