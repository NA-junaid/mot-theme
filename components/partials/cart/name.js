import React from "react";
import useTranslation from "../../../services/use-translation";

function Name() {
    const {name} = useTranslation();
    return (
        <div className="form-group pt-4">
            <input
                type="text"
                className="form-control"
                placeholder={name}
            />
        </div>
    );
}

export default Name;
