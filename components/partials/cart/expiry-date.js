import React from "react";
import useTranslation from "../../../services/use-translation";

function ExpiryDate() {
    const {expiry_date} = useTranslation();
    return (
        <div className="form-group">
            <input
                type="text"
                className="form-control"
                placeholder={expiry_date}
            />
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua.
</p>
        </div>
    );
}

export default ExpiryDate;
