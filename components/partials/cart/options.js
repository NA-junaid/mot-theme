import React from "react";
import useTranslation from "../../../services/use-translation";

function Options() {
    const {choose_an_option} = useTranslation();
    return (
        <div className="form-group">
            <select className="form-control">
    <option value="select">{choose_an_option}</option>
                <option>{choose_an_option}</option>
                <option>{choose_an_option}</option>
            </select>
        </div>
    );
}

export default Options;
