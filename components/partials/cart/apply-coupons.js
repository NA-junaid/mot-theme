import React from "react";
import useTranslation from "../../../services/use-translation";

function ApplyCoupons() {
    const {apply , apply_coupans} = useTranslation();
    return (
        <div className="row mt-4">
            <div className="col-md-8 col-sm-12">
                <div className="apply-coupons p-4">
                    <input
                        type="text"
                        className="form-control"
                        name
                        placeholder={apply_coupans}
                    />
                    <button
                        type="submit"
                        value="Apply"
                        className="btn btn-apply"
                    >
                        {" "}
                        {apply}
                    </button>
                </div>
            </div>
        </div>
    );
}

export default ApplyCoupons;
