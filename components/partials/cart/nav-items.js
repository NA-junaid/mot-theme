import React from "react";
import useTranslation from "../../../services/use-translation";

function NavItems() {
    const {checkout , shipping_information , payment} = useTranslation();
    return (
        <div>
            <ul
                className="nav nav-pills nav-pills-container mt-minus mb-3 nav-justified"
                id="pills-tab"
                role="tablist"
            >
                <li className="nav-item">
                    <a
                        className="nav-link active"
                        id="checkout-tab"
                        data-toggle="pill"
                        href="#checkout"
                        role="tab"
                        aria-controls="checkout"
                        aria-selected="true"
                    >
                        {checkout}
      </a>
                </li>
                <li className="nav-item">
                    <a
                        className="nav-link"
                        id="shipping-tab"
                        data-toggle="pill"
                        href="#shipping"
                        role="tab"
                        aria-controls="shipping"
                        aria-selected="false"
                    >
                        {shipping_information}
      </a>
                </li>
                <li className="nav-item">
                    <a
                        className="nav-link"
                        id="payment-tab"
                        data-toggle="pill"
                        href="#payment"
                        role="tab"
                        aria-controls="payment"
                        aria-selected="false"
                    >
                        {payment}
      </a>
                </li>
            </ul>
        </div>
    );
}

export default NavItems;
