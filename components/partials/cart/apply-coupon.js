import React from "react";
import useTranslation from "../../../services/use-translation";

function ApplyCoupan() {
    const {apply , apply_coupans} = useTranslation();
    return (
        <div>
            <form>
                <input type="text" name placeholder={apply_coupans} />
                <button type="submit" value="Apply" className="btn btn-apply">
                    {apply}
</button>
            </form>
        </div>
    );
}

export default ApplyCoupan;
