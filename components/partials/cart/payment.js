import React from "react";

import {AddCard,Name,Card,ExpiryDate,NetBanking,Options,Knet,PayNow,ReviewOrder} from "./";
import useTranslation from "../../../services/use-translation";

function Payment() {
    const { select_payment_method } = useTranslation();
    return (
        <div
            className="tab-pane fade show active"
            id="payment"
            role="tabpanel"
            aria-labelledby="payment-tab"
        >
            <div className="row align-items-center">
                <div className="col-md-6">
                    <div className="select-payment">
                        <h2>{select_payment_method}</h2>
                        <form>
                            <div className="form-cont">
                                <AddCard />
                                <Name />
                                <Card />
                                <ExpiryDate />
                                <NetBanking />
                                <Options />
                                <Knet />
                            </div>
                            <PayNow />
                        </form>
                    </div>
                </div>
                <ReviewOrder />
            </div>
        </div>
    );
}

export default Payment;
