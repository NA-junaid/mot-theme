import React from "react";
import useTranslation from "../../../services/use-translation";

function ReviewOrder() {
    const { order_review_note_login, login } = useTranslation();
    return (
        <div className="col-md-6">
            <div className="review-order">
                <p>{order_review_note_login}</p>
                <a className="btn btn-primary delivery-here">{login}</a>
            </div>
        </div>
    );
}

export default ReviewOrder;
