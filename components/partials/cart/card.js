import React from "react";
import useTranslation from "../../../services/use-translation";

function Card() {
    const {card} = useTranslation();
    return (
        <div className="form-group">
            <input
                type="text"
                className="form-control"
                placeholder= {card}
            />
        </div>
    );
}

export default Card;
