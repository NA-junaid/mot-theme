import React from "react";
import {NextStep,ShippingInfo} from "./";

function ShippingMethod() {
  return (
    <div
      className="tab-pane fade "
      id="shipping"
      role="tabpanel"
      aria-labelledby="shipping-tab"
    >
      <ShippingInfo />
      <NextStep />
    </div>
  );
}

export default ShippingMethod;
