import React from "react";
import ApplyCoupan from "./apply-coupon";
import useTranslation from "../../../services/use-translation";

function Coupans() {
    const { coupans, price_details } = useTranslation();
    return (
        <div className="col-md-4">
            <div className="coupans-container">
                <h2>{coupans}</h2>
                <ApplyCoupan />
                <div className="price-detail">
                    <h2>{price_details}</h2>
                    <table className="table">
                        <tbody>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                            <tr>
                                <td>Bag Total</td>
                                <td>KD 5789</td>
                            </tr>
                        </tbody>
                    </table>
                    <table className="table">
                        <tbody>
                            <tr>
                                <td>Total</td>
                                <td>KD 5789</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Coupans;
