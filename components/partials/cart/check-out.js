import CartTable from "./cart-table";
import {ApplyCoupons} from "./";
import React from "react";

function CheckOut() {
  return (
    <div
    className="tab-pane fade "
    id="checkout"
    role="tabpanel"
    aria-labelledby="checkout-tab"
  >
    <div className="cart-main-area pr-4 pl-4">
      <div className="row">
        <div className="col-md-12 col-sm-12">
          {/* Form Start */}
          <form action="#">
            <CartTable />
            <ApplyCoupons />
          </form>
          {/* Form End */}
        </div>
      </div>
    </div>
  </div>
  );
}

export default CheckOut;
