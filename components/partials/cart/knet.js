import React from "react";
import useTranslation from "../../../services/use-translation";

function Knet() {
    const {knet} = useTranslation();
    return (
        <div className="custom-control custom-radio custom-control-inline">
            <input
                type="radio"
                className="custom-control-input"
                id="customRadio11"
                name="example"
            />
            <label
                className="custom-control-label"
                htmlFor="customRadio11"
            >
                {knet}
</label>
        </div>
    );
}

export default Knet;
