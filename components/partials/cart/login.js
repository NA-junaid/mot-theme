import React from "react";
import useTranslation from "../../../services/use-translation";

function Login() {
    const { if_already_user_please, login } = useTranslation();
    return (
        <div className="form-group col-md-6">
            <a href="#." className="btn btn-login">
                {if_already_user_please}<span>{login}</span>
            </a>
        </div>
    );
}

export default Login;
