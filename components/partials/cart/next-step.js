import React from "react";
import useTranslation from "../../../services/use-translation";

function NextStep() {
    const {next_step} = useTranslation();
    return (
        <div className="row">
            <div className="col-md-12">
                <div className="next-step">
                    <a href="#.">
                        {next_step}<i className="fa fa-angle-right" />
                    </a>
                </div>
            </div>
        </div>
    );
}

export default NextStep;
