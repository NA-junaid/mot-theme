import React from "react";
import useTranslation from "../../../services/use-translation";

function City() {
    const {state , city_district_town} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={city_district_town}
                />
            </div>
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={state}
                />
            </div>
        </div>
    );
}

export default City;
