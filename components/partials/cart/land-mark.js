import React from "react";
import useTranslation from "../../../services/use-translation";

function LandMark() {
    const {landmark , alternate} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={landmark}
                />
            </div>
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={alternate}
                />
            </div>
        </div>
    );
}

export default LandMark;
