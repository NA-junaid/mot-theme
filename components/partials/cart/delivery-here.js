import React from "react";
import useTranslation from "../../../services/use-translation";

function DeliveryHere() {
    const {save_and_delivery_here} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-6">
                <button
                    type="submit"
                    className="btn btn-primary delivery-here"
                >
                    {save_and_delivery_here}
</button>
            </div>
        </div>
    );
}

export default DeliveryHere;
