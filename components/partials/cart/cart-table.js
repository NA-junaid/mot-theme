import React from "react";
import useTranslation from "../../../services/use-translation";

function CartTable() {
    const { image, product, price, quantity, total, remove } = useTranslation();
    return (
        <div className="table-content table-responsive mb-45">
            <table>
                <thead>
                    <tr>
                        <th className="product-thumbnail">{image}</th>
                        <th className="product-name">{product}</th>
                        <th className="product-price">{price}</th>
                        <th className="product-quantity">{quantity}</th>
                        <th className="product-subtotal">{total}</th>
                        <th className="product-remove">{remove}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="product-thumbnail">
                            <a href="#">
                                <img src="assets/img/shoe.png" alt="cart-image" />
                            </a>
                        </td>
                        <td className="product-name">
                            <a href="#.">
                                Jordon Premium <span>ONLINE SHOE STORE</span>
                            </a>
                        </td>
                        <td className="product-price">
                            <span className="amount">KWD 20</span>
                        </td>
                        <td className="product-quantity">
                            <div className="btn-group">
                                <button type="button" className="prev btn ">
                                    -
                          </button>
                                <button type="button" className="show-number btn ">
                                    1
                          </button>
                                <button type="button" className="next btn ">
                                    +
                          </button>
                            </div>
                        </td>
                        <td className="product-subtotal">KWD 20</td>
                        <td className="product-remove">
                            {" "}
                            <a href="#">
                                <i className="fa fa-times" aria-hidden="true" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td className="product-thumbnail">
                            <a href="#">
                                <img src="assets/img/shoe.png" alt="cart-image" />
                            </a>
                        </td>
                        <td className="product-name">
                            <a href="#.">
                                Jordon Premium <span>ONLINE SHOE STORE</span>
                            </a>
                        </td>
                        <td className="product-price">
                            <span className="amount">KWD 20</span>
                        </td>
                        <td className="product-quantity">
                            <div className="btn-group">
                                <button type="button" className="prev btn ">
                                    -
                          </button>
                                <button type="button" className="show-number btn ">
                                    1
                          </button>
                                <button type="button" className="next btn ">
                                    +
                          </button>
                            </div>
                        </td>
                        <td className="product-subtotal">KWD 20</td>
                        <td className="product-remove">
                            {" "}
                            <a href="#">
                                <i className="fa fa-times" aria-hidden="true" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default CartTable;
