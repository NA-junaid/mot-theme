import React from "react";
import useTranslation from "../../../services/use-translation";

function NetBanking() {
    const {net_banking} = useTranslation();
    return (
        <div className="custom-control custom-radio custom-control-inline mb-3 mt-2">
            <input
                type="radio"
                className="custom-control-input"
                id="customRadio10"
                name="example"
            />
            <label
                className="custom-control-label"
                htmlFor="customRadio10"
            >
                {net_banking}
</label>
        </div>
    );
}

export default NetBanking;
