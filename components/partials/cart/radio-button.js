import React from "react";
import useTranslation from "../../../services/use-translation";

function RadioButton() {
    const {home_delivery , work_delivery} = useTranslation();
    return (
        <div className="form-row">
            <div className="form-group col-md-5">
                <div className="custom-control custom-radio custom-control-inline">
                    <input
                        type="radio"
                        className="custom-control-input"
                        id="customRadio7"
                        name="example"
                    />
                    <label
                        className="custom-control-label"
                        htmlFor="customRadio7"
                    >
                        {home_delivery}
</label>
                </div>
            </div>
            <div className="form-group col-md-7">
                <div className="custom-control custom-radio custom-control-inline">
                    <input
                        type="radio"
                        className="custom-control-input"
                        id="customRadio8"
                        name="example"
                    />
                    <label
                        className="custom-control-label"
                        htmlFor="customRadio8"
                    >
                        {work_delivery}
</label>
                </div>
            </div>
        </div>
    );
}

export default RadioButton;
