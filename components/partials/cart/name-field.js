import React from "react";
import useTranslation from "../../../services/use-translation";

function NameField() {
    const {name , digit_mobile_number} = useTranslation(); 
    return (
        <div className="form-row">
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={name}
                />
            </div>
            <div className="form-group col-md-6">
                <input
                    type="text"
                    className="form-control"
                    placeholder={digit_mobile_number}
                />
            </div>
        </div>
    );
}

export default NameField;
