import React from "react";

function WriteReview({text}) {
    return (
        <div>
            {" "}
            <a href="#" className="btn-primary float-right">
                {text}
                  </a>
        </div>
    );
}

export default WriteReview;
