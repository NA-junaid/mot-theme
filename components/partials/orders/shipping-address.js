import React from "react";
import Address from "./address";
import useTranslation from "../../../services/use-translation";

function ShippingAddress() {
    const {shipping_address} = useTranslation();
    return (
        <div className="row mt-3">
            <div className="col-md-6">
                <Address text={shipping_address}/>
                <Address text={shipping_address}/>
            </div>
            <div className="col-md-6">
                <Address text={shipping_address}/>
            </div>
        </div>
    );
}

export default ShippingAddress;
