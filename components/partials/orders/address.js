import React from "react";

function Address({text}) {
    return (
        <div className="card p-3 mt-3">
            <h2 className="mb-2">{text}</h2>
            <p>Muhammad</p>
            <p>
                Assotech Business Cresterra Plot No-22, Expressway,, Sector 135,
                Noida, Uttar Pradesh 201304
  </p>
        </div>
    );
}

export default Address;
