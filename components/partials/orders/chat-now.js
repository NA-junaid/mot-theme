import React from "react";

function ChatNow({ text }) {
    return (
        <div className="col-md-6 text-sm-right">
            <p className="mb-0">
                {" "}
                <a href="#." className="btn-primary">
                    <i className="icon-bubble align-middle" /> {text}
                </a>
            </p>
        </div>
    );
}

export default ChatNow;
