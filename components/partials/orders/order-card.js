import React from "react";
import ProgressBar from "./progress-bar";
import ChatNow from "./chat-now";
import WriteReview from "./write-review";
import useTranslation from "../../../services/use-translation";

function OrderCard() {
    const {_package,sold_by, delivered_on, standard, qty, chat_now, write_review, order_processing } = useTranslation();
    return (
        <div>
            <div className="card mt-4 pb-5">
                <div className="row justify-content-between align-items-end p-3">
                    <div className="col-md-6">
                        <h5>
                            <i className="icon-present" />{_package}1{" "}
                            <span className="d-block">
                                {sold_by} <a href>Hall Road</a>
                            </span>
                        </h5>
                    </div>
                    <ChatNow text={chat_now}/>
                </div>
                <hr className="m-0" />
                <div className="row d-flex justify-content-between p-3">
                    <div className="col-md-6">
                        <h5>{delivered_on} 11 Jan 2020 23:53:44</h5>
                    </div>
                    <div className="col-md-6 text-sm-right">
                        <p className="mb-0">
                            <i className="icon-present" /> {standard}
                        </p>
                    </div>
                </div>
                <div className="table-content table-responsive  mt-3 mb-3 p-3">
                    <table>
                        <tbody>
                            <tr>
                                <td className="product-thumbnail">
                                    <a href="#">
                                        <img src="assets/img/shoe.png" alt="cart-image" />
                                    </a>
                                </td>
                                <td className="product-name">
                                    <a href="#.">
                                        Jordon Premium <span>ONLINE SHOE STORE</span>
                                    </a>
                                </td>
                                <td className="product-price">
                                    <span className="amount">KWD 20</span>
                                </td>
                                <td className="product-quantity">
                                    <p>
                                        <b>{qty}:</b> 1
                  </p>
                                </td>
                                <td className="product-write-review">
                                <WriteReview text={write_review} />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <ProgressBar />
            </div>
        </div>
    );
}

export default OrderCard;
