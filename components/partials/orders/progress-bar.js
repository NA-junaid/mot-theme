import React from "react";
import useTranslation from "../../../services/use-translation";

function ProgressBar() {
    const { order_processing, shipped, delivered, thank_you_mot} = useTranslation();
    return (
        <div className="col-md-12 text-center">
            {/* Add class 'active' to progress */}
            <ol className="progtrckr" data-progtrckr-steps={5}>
                <li className="progtrckr-done">{order_processing}</li>
                {/*
*/}
                <li className="progtrckr-done">{shipped}</li>
                {/*
*/}
                <li className="progtrckr-todo">{delivered}</li>
            </ol>
            <div className="card delivery_message p-4">
                {/* <div class="arrow-up"></div> */}
                <p>
                    14 Jan 2019 -22:40{" "}
                    <span className="pl-3 font-weight-bold">
                        {thank_you_mot}
</span>
                </p>
            </div>
        </div>
    );
}

export default ProgressBar;
