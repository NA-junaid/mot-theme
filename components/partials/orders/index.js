import Address from './address';
import ChatNow from './chat-now';
import OrderCard from './order-card';
import OrderDetails from './order-details';
import ProgressBar from './progress-bar';
import ShippingAddress from './shipping-address';
import WriteReview from './write-review';

export {
    Address,
    ChatNow,
    OrderCard,
    OrderDetails,
    ProgressBar,
    ShippingAddress,
    WriteReview
};