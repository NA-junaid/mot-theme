import React from "react";
import useTranslation from "../../../services/use-translation";

function OrderDetails() {
  const { order_details, order, placed_on, total } = useTranslation();
  return (
    <div>
      <h2>{order_details}</h2>
      <div className="row mt-4">
        <div className="col-md-12">
          <p className="float-left">
            {order} <b className="color-primary">#098765213898262</b>{" "}
            <span className="d-block">{placed_on} 11 Jan 2020 23:53:44</span>
          </p>
          <p className="float-right">
            <b>{total}:</b> <span>Rs. 300</span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default OrderDetails;
