import React from "react";
import useTranslation from "../../../services/use-translation";

function LoginForm() {
    const {login,email,email_password,you_agree, forgot_password } = useTranslation();
    return (
        <div>
            <form className="form-verify or">
                <h2 className="text-center mb-3">{login}</h2>
                <div className="form-body">
                    <input type="text" className="form-control" placeholder={email} />
                    <input
                        type="text"
                        className="form-control mt-3"
                        placeholder={email_password}
                    />
                    <div className="custom-control custom-checkbox mt-4">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="customCheck"
                        />
                        <label className="custom-control-label" htmlFor="customCheck">
                        {you_agree}
              </label>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary delivery-here">
                {login}
          </button>
                <button type="submit" className="btn btn-default delivery-here">
                {forgot_password}
          </button>
            </form>
        </div>
    );
}

export default LoginForm;
