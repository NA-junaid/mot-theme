import React from "react";
import useTranslation from "../../../services/use-translation";

function RegisterForm() {
    const { register, user_name, email, password, reenter_password, } = useTranslation();
    return (
        <div>
            <form className="form-verify">
                <h2 className="text-center mb-3">{register}</h2>
                <div className="form-body">
                    <input
                        type="text"
                        className="form-control"
                        placeholder={user_name}
                    />
                    <input
                        type="text"
                        className="form-control mt-3"
                        placeholder={email}
                    />
                    <input
                        type="text"
                        className="form-control mt-3"
                        placeholder={password}
                    />
                    <input
                        type="text"
                        className="form-control mt-3"
                        placeholder={reenter_password}
                    />
                </div>
                <button type="submit" className="btn btn-primary delivery-here">
                    {register}
                </button>
            </form>
        </div>
    );
}

export default RegisterForm;
