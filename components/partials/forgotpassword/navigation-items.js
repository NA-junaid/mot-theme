import React from "react";
import useTranslation from "../../../services/use-translation";

function NavigationItems() {
    const {forgot_password,authentication,new_password } = useTranslation();
    return (
        <div>
            <ul
                className="mt-minus nav nav-pills nav-pills-container nav-justified"
                id="pills-tab"
                role="tablist"
            >
                <li className="nav-item">
                    <a
                        className="nav-link active"
                        id="forgotpass-tab"
                        data-toggle="pill"
                        href="#forgotpass"
                        role="tab"
                        aria-controls="forgotpass"
                        aria-selected="true"
                    >
                        {forgot_password}
      </a>
                </li>
                <li className="nav-item">
                    <a
                        className="nav-link"
                        id="athentication-tab"
                        data-toggle="pill"
                        href="#athentication"
                        role="tab"
                        aria-controls="athentication"
                        aria-selected="false"
                    >
                        {authentication}
      </a>
                </li>
                <li className="nav-item">
                    <a
                        className="nav-link"
                        id="newpass-tab"
                        data-toggle="pill"
                        href="#newpass"
                        role="tab"
                        aria-controls="newpass"
                        aria-selected="false"
                    >
                       {new_password}
      </a>
                </li>
            </ul>
        </div>
    );
}

export default NavigationItems;
