import React from "react";
import useTranslation from "../../../services/use-translation";

function PasswordForgot() {
    const {forgot_password,email_or_mobile,_continue} = useTranslation();
    return (
        <div>
            <form className="form-verify m-5">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <h2 className="text-center mb-3">{forgot_password}</h2>
                        <div className="form-body">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
                            <input
                                type="text"
                                className="form-control mt-3"
                                placeholder={email_or_mobile}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary delivery-here">
                        {_continue}
            </button>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default PasswordForgot;
