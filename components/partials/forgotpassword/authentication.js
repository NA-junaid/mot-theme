import React from "react";
import useTranslation from "../../../services/use-translation";

function Authentication() {
    const {authentication_required,enter_otp, _continue, resend } = useTranslation();
    return (
        <div>
            <form className="form-verify m-5">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <h2 className="text-center mb-3">{authentication_required}</h2>
                        <div className="form-body">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
                            <input
                                type="text"
                                className="form-control mt-3"
                                placeholder={enter_otp}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary delivery-here">
                        {_continue}
            </button>
                        <button type="submit" className="btn btn-default delivery-here">
                        {resend}
            </button>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default Authentication;
