import React from "react";
import useTranslation from "../../../services/use-translation";

function ResetPassword() {
    const { reset_password, reenter_new_password, enter_new_password } = useTranslation();
    return (
        <div>
            <form className="form-verify m-5">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <h2 className="text-center mb-3">{reset_password}</h2>
                        <div className="form-body">
                            <input
                                type="text"
                                className="form-control"
                                placeholder={enter_new_password}
                            />
                            <input
                                type="text"
                                className="form-control mt-3"
                                placeholder={reenter_new_password}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary delivery-here">
                            {reset_password}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default ResetPassword;
