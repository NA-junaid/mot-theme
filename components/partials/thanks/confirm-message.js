import React from "react";
import useTranslation from "../../../services/use-translation";

function ConfirmMessage() {
    const {order_confirmed , your_order_id} = useTranslation();
    return (
        <div>
            <p>
               {order_confirmed}
            <br />
               {your_order_id}<span>#98654637</span>
            </p>
        </div>
    );
}

export default ConfirmMessage;
