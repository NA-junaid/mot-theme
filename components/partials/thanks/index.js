import ConfirmMessage from './confirm-message';
import GoHome from './go-home';
import ThanksImage from './thanks-image';
import Title from './title';

export{
    ConfirmMessage,
    GoHome,
    ThanksImage,
    Title
};