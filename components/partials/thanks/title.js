import React from "react";
import useTranslation from "../../../services/use-translation";

function Title() {
    const {thank_you} = useTranslation();
    return (
        <div>
            <h1 className="mt-4 mb-3">{thank_you}</h1>
        </div>
    );
}

export default Title;

