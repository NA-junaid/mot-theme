import React from "react";
import useTranslation from "../../../services/use-translation";

function GoHome() {
    const {go_home} = useTranslation();
    return (
        <div>
            <button
                type="submit"
                className="btn btn-primary delivery-here mt-5 mr-4"
            >
                {go_home}
          </button>
        </div>
    );
}

export default GoHome;
