import React from "react";

function ThanksImage() {
    return (
        <div>
            <img src="assets/img/smile.png" />
        </div>
    );
}

export default ThanksImage;
