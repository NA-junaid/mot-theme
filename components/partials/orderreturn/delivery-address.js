import React from "react";
import useTranslation from "../../../services/use-translation";

function DeliverAddress() {
    const { delivery_address, phone_number } = useTranslation();
    return (
        <div className="row mt-4">
            <div className="col-md-12">
                <div className="card p-3">
                    <h2 className="mb-2">{delivery_address}</h2>
                    <p>
                        <strong>Muhammad</strong>
                    </p>
                    <p>
                        House 6 Thermal Rail Gate Opp Assam Petrol Pump Honda Showroom House
                        6 Thermal Rail Gate Opp Assam Petrol Pump Honda Showroom, Opp - Jama
                        Masjid Tinsukia - 786125, Assam
</p>
                    <h6 className="mt-3">{phone_number}</h6>
                    <p>8010226776, 7664019545</p>
                </div>
            </div>
        </div>
    );
}

export default DeliverAddress;
