import React from "react";
import useTranslation from "../../../services/use-translation";
import DeliveredOn from "./delivered-on";

function PackageDescription() {
    const { sold_by, need_help } = useTranslation();
    return (
        <div className="card mt-4 pb-5">
            <div className="row justify-content-between align-items-end p-3">
                <div className="col-md-6">
                    <h5>
                        <i className="icon-present" /> Package1{" "}
                        <span className="d-block">
                            {sold_by} <a href>Hall Road</a>
                        </span>
                    </h5>
                </div>
                <div className="col-md-6 text-sm-right">
                    <p className="mb-0">
                        {" "}
                        <a href="#." className="btn-primary">
                            <i className="icon-bubble align-middle" /> {need_help}
                        </a>
                    </p>
                </div>
            </div>
            <hr className="m-0" />
            <DeliveredOn />
        </div>
    );
}

export default PackageDescription;
