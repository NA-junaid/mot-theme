import React from "react";
import useTranslation from "../../../services/use-translation";

function DeliveredOn() {
    const { delivered_on, cancelled, qty, order, agent_not_reached } = useTranslation();
    return (
        <div>
            <div className="row d-flex justify-content-between p-3">
                <div className="col-md-6">
                    <h5>{delivered_on} 11 Jan 2020 23:53:44</h5>
                    <p className="mt-2 mb-2">Color: Brown Size: L</p>
                </div>
                <div className="col-md-6 text-sm-right">
                    <p className="mb-0">
                        <i className="icon-close" /> {cancelled}
                    </p>
                </div>
            </div>
            <div className="table-content table-responsive table-spacer mr-6 ml-6 mt-3 mb-3 p-3">
                <table>
                    <tbody>
                        <tr>
                            <td className="product-thumbnail">
                                <a href="#">
                                    <img src="assets/img/shoe.png" alt="cart-image" />
                                </a>
                            </td>
                            <td className="product-name">
                                <a href="#.">
                                    Jordon Premium <span>ONLINE SHOE STORE</span>
                                </a>
                            </td>
                            <td className="product-price">
                                <span className="amount">KWD 20</span>
                            </td>
                            <td className="product-quantity">
                                <p>
                                    <b>{qty}:</b> 1
</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="col-md-12 text-center">
                {/* Add class 'active' to progress */}
                <ol className="progtrckr" data-progtrckr-steps={5}>
                    <li className="progtrckr-done">{order} </li>
                    <li className="progtrckr-todo">{cancelled}</li>
                </ol>
                <div className="card delivery_message p-4">
                    {/* <div class="arrow-up"></div> */}
                    <p>
                        14 Jan 2019 -22:40{" "}
                        <span className="pl-3 font-weight-bold">
                            {agent_not_reached}
                        </span>
                    </p>
                </div>
            </div>
        </div>
    );
}

export default DeliveredOn;
