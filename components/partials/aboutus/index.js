import About from './about';
import WhyChoose from './why-choose';
import Service from './service';
import ShopInfo from './shop-info';
export { About, WhyChoose, Service, ShopInfo };