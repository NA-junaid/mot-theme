import useTranslation from "../../../services/use-translation";

export default function ShopInfo() {
    const {all_day,support, free_delivery, day_return,} = useTranslation();
    return (
        <div className="section pb_70 ">
            <div className="container">
                <div className="row no-gutters">
                    <div className="col-lg-4">
                        <div className="icon_box icon_box_style1">
                            <div className="icon">
                                <i className="icon-plane" />
                            </div>
                            <div className="icon_box_content">
                                <h5>{free_delivery}</h5>
                                <p>
                                    If you are going to use of Lorem, you need to be sure there
                                    anything
                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="icon_box icon_box_style1">
                            <div className="icon">
                                <i className="icon-refresh" />
                            </div>
                            <div className="icon_box_content">
                                <h5>30 {day_return}</h5>
                                <p>
                                    If you are going to use of Lorem, you need to be sure there
                                    anything
                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="icon_box icon_box_style1">
                            <div className="icon">
                                <i className="icon-call-out" />
                            </div>
                            <div className="icon_box_content">
                                <h5>{all_day} {support}</h5>
                                <p>
                                    If you are going to use of Lorem, you need to be sure there
                                    anything
                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}