import useTranslation from "../../../services/use-translation";

export default function Service() {
    const { all_day, customer_services,oneclick_contactus} = useTranslation();
    return (
        <div className="container">
            <div className="row about-service-mt">
                <div className="col-lg-12">
                    <div className="cont">
                        <div className="desc">
                            <i />
                            <div className="info">
                                <b>{all_day}</b>
                                <p>{customer_services}</p>
                            </div>
                        </div>
                        <div className="btns">
                            <a
                                href="https://www.banggood.in/Contact-Us_hi111"
                                collected={1}
                                className
                            >
                                {oneclick_contactus}
                            </a>
                        </div>
                    </div>
                    <img src="assets/img/about-service.jpg" />
                </div>
            </div>
        </div>
    );
}