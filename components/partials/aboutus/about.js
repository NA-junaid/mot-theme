export default function About({text}) {
    return (
        <div className="section">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div
                            className="about_img scene mb-4 mb-lg-0"
                            style={{
                                transform: "translate3d(0px, 0px, 0px)",
                                transformStyle: "preserve-3d",
                                backfaceVisibility: "hidden",
                                position: "relative"
                            }}
                        >
                            <img src="assets/images/about_img.jpg" alt="about_img" />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="heading_s1">
                            <h2>{text}</h2>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            consequuntur quibusdam enim expedita sed nesciunt incidunt
                            accusamus adipisci officia libero laboriosam!
            </p>
                        <p>
                            Proin gravida nibh vel velit auctor aliquet. nec sagittis sem nibh
                            id elit. Duis sed odio sit amet nibh vultate cursus a sit amet
                            mauris. Duis sed odio sit amet nibh vultate cursus a sit amet
                            mauris.
            </p>
                    </div>
                </div>
            </div>
        </div>
    );
}