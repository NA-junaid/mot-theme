import useTranslation from "../../../services/use-translation";

export default function WhyChoose() {
    const {why_choose, quality_savings, gloval_warehouse, payment_security} = useTranslation();
    return (
        <div className="section bg_light_blue2 pb_70">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-6 col-md-8">
                        <div className="heading_s1 text-center">
                            <h2>{why_choose}?</h2>
                        </div>
                        <p className="text-center leads">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor incididunt ut labore.
            </p>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-4 col-sm-6">
                        <div className="icon_box icon_box_style4 box_shadow1">
                            <div className="icon">
                                <i className="icon-badge" />
                            </div>
                            <div className="icon_box_content">
                                <h5>{quality_savings}</h5>
                                <p>
                                    There are many variations of passages of Lorem Ipsum
                                    available, but the majority have suffered alteration in some
                                    form
                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <div className="icon_box icon_box_style4 box_shadow1">
                            <div className="icon">
                                <i className="fa fa-building-o" />
                            </div>
                            <div className="icon_box_content">
                                <h5>{gloval_warehouse}</h5>
                                <p>
                                    There are many variations of passages of Lorem Ipsum
                                    available, but the majority have suffered alteration in some
                                    form
                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <div className="icon_box icon_box_style4 box_shadow1">
                            <div className="icon">
                                <i className="icon-wallet" />
                            </div>
                            <div className="icon_box_content">
                                <h5>{payment_security}</h5>
                                <p>
                                    There are many variations of passages of Lorem Ipsum
                                    available, but the majority have suffered alteration in some
                                    form
                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}