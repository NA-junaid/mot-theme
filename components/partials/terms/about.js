import React from "react";
import useTranslation from "../../../services/use-translation";

function About() {
    const {terms_there} = useTranslation();
    return (
        <div>
            <h6>{terms_there}</h6>
            <p>
                It is a long established fact that a reader will be distracted by
                the readable content of a page when looking at its layout. The point
                of using Lorem Ipsum is that it has a more-or-less normal
                distribution of letters, as opposed to using 'Content here, content
                here', making it look like readable English. Many desktop publishing
                packages and web page editors now use Lorem Ipsum as their default
                model text, and a search for 'lorem ipsum' will uncover many web
                sites still in their infancy. Various versions have evolved over the
                years, sometimes by accident, sometimes on purpose injected humour
                and the like
          </p>
            <ol>
                <li>
                    Ut enim ad minima veniam, quis nostrum exercitationem ullam
                    corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                    consequatur
            </li>
                <li>
                    Quis autem vel eum iure reprehenderit qui in ea voluptate velit
                    esse quam nihil molestiae consequatur, vel illum qui dolorem eum
                    fugiat quo voluptas nulla pariatur
            </li>
                <li>
                    Et harum quidem rerum facilis est et expedita distinctio. Nam
                    libero tempore, cum soluta nobis est eligendi optio cumque nihil
                    impedit quo minus id quod maxime placeat facere possimus, omnis
              voluptas assumenda est, omnis dolor repellendus.{" "}
                </li>
                <li>
                    Temporibus autem quibusdam et aut officiis debitis aut rerum
                    necessitatibus saepe eveniet ut et voluptates repudiandae sint et
                    molestiae non recusandae.
            </li>
            </ol>
            <p>
                There are many variations of passages of Lorem Ipsum available, but
                the majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even slightly
                believable. If you are going to use a passage of Lorem Ipsum, you
                need to be sure there isn't anything embarrassing hidden in the
                middle of text.
          </p>
            <hr />
        </div>
    );
}

export default About;
