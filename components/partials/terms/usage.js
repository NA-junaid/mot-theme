import React from "react";
import useTranslation from "../../../services/use-translation";

function Usage() {
  const {terms_why} = useTranslation();
  return (
    <div>
      <h6>{terms_why}</h6>
      <p>
        Contrary to popular belief, Lorem Ipsum is not simply random text.
        It has roots in a piece of classical Latin literature from 45 BC,
        making it over 2000 years old. Richard McClintock, a Latin professor
        at Hampden-Sydney College in Virginia, looked up one of the more
        obscure Latin words
          </p>
      <p>
        All the Lorem Ipsum generators on the Internet tend to repeat
        predefined chunks as necessary, making this the first true generator
        on the Internet. It uses a dictionary of over 200 Latin words,
        combined with a handful of model sentence structures, to generate
        Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is
        therefore always free from repetition, injected humour, or
        non-characteristic words etc.
          </p>
      <ul>
        <li>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
          enim ad minim veniam, quis nostrud exercitation ullamco laboris
          nisi ut aliquip ex ea commodo consequat.
            </li>
        <li>
          Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
          cupidatat non proident, sunt in culpa qui officia deserunt mollit
          anim id est laborum.
            </li>
        <li>
          No one rejects, dislikes, or avoids pleasure itself, because it is
          pleasure, but because those who do not know how to pursue pleasure
          rationally encounter consequences that are extremely painful.
            </li>
      </ul>
    </div>
  );
}

export default Usage;
