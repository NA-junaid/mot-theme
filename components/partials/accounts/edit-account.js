import React from "react";
import useTranslation from "../../../services/use-translation";

function EditAccount() {
    const {edit_account , name , number , email , date_of_birth , update_information} = useTranslation();
  return (
    <form className="account-form">
    <div className="form-header p-3">
      <h2 className="mb-0">{edit_account}</h2>
    </div>
    <div className="form-body">
      <div className="form-row">
        <div className="form-group col-md-6">
          <input
            type="text"
            className="form-control"
            placeholder={name}
          />
        </div>
        <div className="form-group col-md-6">
          <input
            type="text"
            className="form-control"
            placeholder={number}
          />
        </div>
      </div>
      <div className="form-row">
        <div className="form-group col-md-6">
          <input
            type="text"
            className="form-control"
            placeholder={email}
          />
        </div>
        <div className="form-group col-md-6">
          <input
            type="text"
            className="form-control"
            placeholder={date_of_birth}
          />
        </div>
      </div>
      <div className="form-row">
        <div className="form-group col-md-6">
          <button
            type="submit"
            className="btn btn-primary delivery-here"
          >
            {update_information}
          </button>
        </div>
      </div>
    </div>
  </form>
  );
}

export default EditAccount;
