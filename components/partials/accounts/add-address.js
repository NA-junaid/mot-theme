import React from "react";
import useTranslation from "../../../services/use-translation";

function AddAddress() {
    const { add_address, full_name, email, mobile_number, pincode, street, city, state, country } = useTranslation();
    return (
        <form className="account-form">
            <div className="form-header p-3">
                <h2 className="mb-0">{add_address}</h2>
            </div>
            <div className="form-body">
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={full_name}
                        />
                    </div>
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={email}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={mobile_number}
                        />
                    </div>
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={pincode}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={street}
                        />
                    </div>
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={city}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={state}
                        />
                    </div>
                    <div className="form-group col-md-6">
                        <input
                            type="text"
                            className="form-control"
                            placeholder={country}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <button
                            type="submit"
                            className="btn btn-primary delivery-here"
                        >
                            + {add_address}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    );
}

export default AddAddress;
