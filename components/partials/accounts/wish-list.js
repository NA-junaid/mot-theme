import React from "react";
import useTranslation from "../../../services/use-translation";

function WishList() {
    const { image, product, price, stock_status, remove } = useTranslation();
    return (
        <div className="table-content table-responsive mb-45 wishlist tbUser">
            <table>
                <thead>
                    <tr>
                        <th className="product-thumbnail">{image}</th>
                        <th className="product-name">{product}</th>
                        <th className="product-price">{price}</th>
                        <th className="product-quantity">{stock_status}</th>
                        <th className="product-remove">{remove}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="product-thumbnail">
                            <a href="#">
                                <img
                                    src="assets/img/products/img1.jpg"
                                    alt="cart-image"
                                />
                            </a>
                        </td>
                        <td className="product-name">
                            <a href="#.">
                                Jordon Premium <span>ONLINE SHOE STORE</span>
                            </a>
                        </td>
                        <td className="product-price">
                            <span className="amount">KWD 20</span>
                        </td>
                        <td className="product-quantity">
                            <span className="badge badge-pill badge-success p-2">
                                In Stock
            </span>
                        </td>
                        <td className="product-remove">
                            {" "}
                            <a href="#" className="btnDelete">
                                <i className="fa fa-times" aria-hidden="true" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default WishList;
