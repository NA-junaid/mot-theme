import AddAddress from './add-address';
import AddressBook from './address-book';
import ChangePassword from './change-password';
import EditAccount from './edit-account';
import MyCancellation from './my-cancellation';
import Sidebar from './side-bar';
import WishList from './wish-list';

export {AddAddress,AddressBook,ChangePassword,EditAccount,MyCancellation,Sidebar,WishList};