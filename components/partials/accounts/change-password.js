import React from "react";
import useTranslation from "../../../services/use-translation";

function ChangePassword() {
    const { change_password, enter_new_password, reenter_new_password } = useTranslation();
    return (
        <div>
            <form className="account-form">
                <div className="form-header p-3">
                    <h2 className="mb-0">{change_password}</h2>
                </div>
                <div className="form-body">
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <input
                                type="text"
                                className="form-control"
                                placeholder={enter_new_password}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <input
                                type="text"
                                className="form-control"
                                placeholder={reenter_new_password}
                            />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <button
                                type="submit"
                                className="btn btn-primary delivery-here"
                            >
                                {change_password}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default ChangePassword;
