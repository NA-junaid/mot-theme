import React from "react";
import useTranslation from "../../../services/use-translation";

function AddressBook() {
    const {address , home_address , edit , office_address , shop_address , add_address} = useTranslation();
  return (
    <form className="account-form">
    <div className="form-header p-3">
      <h2 className="mb-0">{address}</h2>
    </div>
    <div className="form-body billing">
      <div className="row">
        <div className="col-md-4">
          <div className="custom-control custom-radio custom-control-inline">
            <input
              type="radio"
              className="custom-control-input"
              id="customRadio5"
              name="example"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadio5"
            >
              {home_address}
            </label>
          </div>
          <hr />
          <p>
            <span>Nabeel Khan</span>
          </p>
          <p>Bangladesh</p>
          <p>Bayazid Hasan</p>
          <p>Siayana</p>
          <p>Bugrasi</p>
          <p>Basi</p>
          <p>Gao</p>
          <a className="btn-edit" href="#.">
            {edit}
          </a>
          <a className="btn-delete" href="#.">
            Delete
          </a>
        </div>
        <div className="col-md-4">
          <div className="custom-control custom-radio custom-control-inline">
            <input
              type="radio"
              className="custom-control-input"
              id="customRadio1"
              name="example"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadio1"
            >
              {office_address}
            </label>
          </div>
          <hr />
          <p>
            <span>Chand Khan</span>
          </p>
          <p>Bangladesh</p>
          <p>Bayazid Hasan</p>
          <p>Siayana</p>
          <p>Bugrasi</p>
          <p>Basi</p>
          <p>Gao</p>
          <a className="btn-edit" href="#.">
            {edit}
          </a>
          <a className="btn-delete" href="#.">
            Delete
          </a>
        </div>
        <div className="col-md-4">
          <div className="custom-control custom-radio custom-control-inline">
            <input
              type="radio"
              className="custom-control-input"
              id="customRadio2"
              name="example"
            />
            <label
              className="custom-control-label"
              htmlFor="customRadio2"
            >
              {shop_address}
            </label>
          </div>
          <hr />
          <p>
            <span>Mahtab Khan</span>
          </p>
          <p>Bangladesh</p>
          <p>Bayazid Hasan</p>
          <p>Siayana</p>
          <p>Bugrasi</p>
          <p>Basi</p>
          <p>Gao</p>
          <a className="btn-edit" href="#.">
            {edit}
          </a>
          <a className="btn-delete" href="#.">
            Delete
          </a>
        </div>
      </div>
      <div className="form-row">
        <div className="form-group col-md-6">
          <button
            type="submit"
            className="btn btn-primary delivery-here mt-5"
          >
            + {add_address}
          </button>
        </div>
      </div>
    </div>
  </form>
  );
}

export default AddressBook;
