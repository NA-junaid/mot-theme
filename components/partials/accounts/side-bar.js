import React from "react";
import useTranslation from "../../../services/use-translation";

function Sidebar() {
    const {edit_account , change_password , address_book , add_address , wish_list , my_cancelation} = useTranslation();
  return (
    <div className="col-sm-3 user-sidebar">
    <div
      className="nav flex-column nav-pills text-center user-nav pt-5"
      id="sidebar-admin"
      role="tablist"
      aria-orientation="vertical"
    >
      <div className="avatar">
        <img
          className="rounded-circle"
          src="assets/img/avatar.png"
          width={100}
          height={100}
          alt
        />
      </div>
      <div className="content mt-2 mb-4">
        <h2>Tyler Marker</h2>
        <span>457 896 567</span>
      </div>
      <a
        className="nav-link active show user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#editaccount"
        role="tab"
        aria-controls="editaccount"
        aria-selected="true"
      >
        {edit_account}
      </a>
      <a
        className="nav-link user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#changepass"
        role="tab"
        aria-controls="changepass"
        aria-selected="true"
      >
        {change_password}
      </a>
      <a
        className="nav-link user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#addressbook"
        role="tab"
        aria-controls="addressbook"
        aria-selected="false"
      >
        {address_book}
      </a>
      <a
        className="nav-link user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#addaddress"
        role="tab"
        aria-controls="addaddress"
        aria-selected="false"
      >
        {add_address}
      </a>
      <a
        className="nav-link user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#Wishlist"
        role="tab"
        aria-controls="Wishlist"
        aria-selected="false"
      >
        {wish_list}
      </a>
      <a
        className="nav-link user-nav-item user-sidebar-item"
        data-toggle="pill"
        href="#mycancelation"
        role="tab"
        aria-controls="mycancelation"
        aria-selected="false"
      >
        {my_cancelation}
      </a>
    </div>
  </div>
  );
}

export default Sidebar;
