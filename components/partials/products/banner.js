import React from "react";

function Banner() {
  return (
    <div className="container-fluid product_banners">
      <div className="row no-gutters">
        <div className="col-md-7">
          <img src="assets/img/productBanner1.jpg" />
        </div>
        <div className="col-md-5">
          <img src="assets/img/productBanner2.jpg" />
        </div>
      </div>
    </div>
  );
}

export default Banner;
