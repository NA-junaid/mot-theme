import React from "react";
import useTranslation from "../../../services/use-translation";

function Sorting() {
  const {sort_by,_new,old} = useTranslation();
  return (
    <div className="col-md-3">
      <div className="sortby">
        <select className="form-control" id="exampleFormControlSelect1">
          <option>{sort_by} : {_new}</option>
          <option>{sort_by} : {old}</option>
          <option>{sort_by} : {_new}</option>
          <option>{sort_by} : {old}</option>
          <option>{sort_by} : {_new}</option>
        </select>
      </div>
    </div>
  );
}

export default Sorting;
