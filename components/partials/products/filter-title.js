import React from "react";

function FilterTitle({text}) {
  return (
    <div className="col-md-3">
      <div className="d-flex justify-content-between refresh">
        <h5>{text}</h5>
        <span>
          <a href="#">
            <i className="fa fa-refresh" />
          </a>
        </span>
      </div>
    </div>
  );
}

export default FilterTitle;
