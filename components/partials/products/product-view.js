import React from "react";

function ProductView() {
  return (
    <div className="col-md-3">
      <div className="views">
        <a href="#" className="thumbview">
          <i className="fa fa-th " />
        </a>
        <a href="#" className="listview">
          <i className="fa fa-align-left" />
        </a>
      </div>
    </div>
  );
}

export default ProductView;
