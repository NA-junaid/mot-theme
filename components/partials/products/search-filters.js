import React from "react";
import FilterTitle from './filter-title';
import Sorting from './sorting';
import PerPage from './per-page';
import ProductView from './product-view';
import useTranslation from "../../../services/use-translation";


function SearchFilters(props) {
  const {filters} = useTranslation();
  return (
    <div className="container-fluid products_filters">
    <div className="row no-gutters">
        <FilterTitle text={filters}/>
        <Sorting />
        <PerPage />
        <ProductView />
    </div>
  </div>
  );
}

export default SearchFilters;
