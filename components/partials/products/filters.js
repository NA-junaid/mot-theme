import React from "react";
import FilterGroup from './filter-group';

function Filters() {
  return (
    <div className="products_sidebar_menu">
      {/*Accordion wrapper*/}
      <div
        className="accordion md-accordion"
        id="accordionEx"
        role="tablist"
        aria-multiselectable="true"
      >
      <FilterGroup />
        
      </div>
      {/* Accordion wrapper */}
    </div>
  );
}

export default Filters;
