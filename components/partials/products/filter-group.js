import React from "react";
import useTranslation from "../../../services/use-translation";

function FilterGroup() {
  const {categories,price,brand,color,size} = useTranslation();
  return (
    <div>
      <div className="card">
        <div className="card-header" role="tab" id="headingOne1">
          <a
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#collapseOne1"
            aria-expanded="true"
            aria-controls="collapseOne1"
          >
            <h5 className="mb-0">
              {categories}
              <i className="fa fa-angle-down rotate-icon" />
            </h5>
          </a>
        </div>
        <div
          id="collapseOne1"
          className="collapse show"
          role="tabpanel"
          aria-labelledby="headingOne1"
          data-parent="#accordionEx"
        >
          <div className="card-body">
            <ul className="categores_list">
              <li>
                <a href="#">
                  Heels <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Flats <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Casual Shoes <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Sports Shoes <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Flip Flops <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Sandals <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Sports Sandals <span>12690</span>
                </a>
              </li>
              <li>
                <a href="#">
                  Formal Shoes <span>12690</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>


      <div className="card">

        <div className="card-header price_tag" role="tab" id="headingTwo2">
          <a
            className="collapsed"
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#collapseTwo2"
            aria-expanded="false"
            aria-controls="collapseTwo2"
          >
            <h5 className="mb-0">
            {price} <i className="fa fa-angle-down rotate-icon" />
            </h5>
          </a>
        </div>
        {/* Card body */}
        <div
          id="collapseTwo2"
          className="collapse"
          role="tabpanel"
          aria-labelledby="headingTwo2"
          data-parent="#accordionEx"
        >
          <div className="card-body slider_range">
            <div className="range_Slider">
              <input
                type="text"
                className="js-range-slider"
                name="my_range"
                defaultValue
              />
            </div>
          </div>
        </div>
      </div>

      <div className="card">
        {/* Card header */}
        <div className="card-header" role="tab" id="headingThree3">
          <a
            className="collapsed"
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#collapseThree3"
            aria-expanded="false"
            aria-controls="collapseThree3"
          >
            <h5 className="mb-0">
            {brand} <i className="fa fa-angle-down rotate-icon" />
            </h5>
          </a>
        </div>
        {/* Card body */}
        <div
          id="collapseThree3"
          className="collapse"
          role="tabpanel"
          aria-labelledby="headingThree3"
          data-parent="#accordionEx"
        >
          <div className="card-body">
            <div className="brands">
              <form>
                <div className="form-group">
                  <input type="checkbox" id="lifestyle" />
                  <label htmlFor="Lifestyle">Lifestyle</label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="Running" />
                  <label htmlFor="Running">Running</label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="Training Gym" />
                  <label htmlFor="Training Gym">Training Gym</label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="Basketball" />
                  <label htmlFor="Basketball">Basketball </label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="Cricket" />
                  <label htmlFor="Cricket">Cricket</label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="Running" />
                  <label htmlFor="Running">Running</label>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div className="card">

        <div className="card-header" role="tab" id="headingThree3">
          <a
            className="collapsed"
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#Color"
            aria-expanded="false"
            aria-controls="Color"
          >
            <h5 className="mb-0">
            {color} <i className="fa fa-angle-down rotate-icon" />
            </h5>
          </a>
        </div>
        {/* Card body */}
        <div
          id="Color"
          className="collapse"
          role="tabpanel"
          aria-labelledby="headingThree3"
          data-parent="#accordionEx"
        >
          <div className="card-body ">
            <div className="choose_color">
              <form>
                <div className="form-group">
                  <input type="checkbox" id="Red" />
                  <label className="green" htmlFor="Red">
                    Red
                  </label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="yello" />
                  <label className="yellow" htmlFor="yello">
                    Yello
                  </label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="red" />
                  <label className="red" htmlFor="red">
                    Yello
                  </label>
                </div>
                <div className="form-group">
                  <input type="checkbox" id="orange" />
                  <label className="orange" htmlFor="orange">
                    Orange
                  </label>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div className="card">

        <div className="card-header" role="tab" id="headingThree3">
          <a
            className="collapsed"
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#Size"
            aria-expanded="false"
            aria-controls="Size"
          >
            <h5 className="mb-0">
            {size} <i className="fa fa-angle-down rotate-icon" />
            </h5>
          </a>
        </div>
        {/* Card body */}
        <div
          id="Size"
          className="collapse"
          role="tabpanel"
          aria-labelledby="headingThree3"
          data-parent="#accordionEx"
        >
          <div className="card-body">
            <div className="choose_color">
              <form>
                <input
                  type="text"
                  placeholder="40 inch"
                  name
                  className="form-control"
                />
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  );
}

export default FilterGroup;
