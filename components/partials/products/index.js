import Banner from './banner';
import FilterTitle from './filter-title';
import Filters from './filters';
import Pagination from './pagination';
import PerPage from './per-page';
import ProductView from './product-view';
import SearchFilters from './search-filters';
import Sorting from './sorting';

export {
  Banner,
  FilterTitle,
  Filters,
  Pagination,
  PerPage,
  ProductView,
  SearchFilters,
  Sorting
};