import React from "react";
import useTranslation from "../../../services/use-translation";

function PerPage() {
  const {show_20,show_30,show_40} = useTranslation();
  return (
    <div className="col-md-3">
      <div className="showPro">
        <select className="form-control" id="exampleFormControlSelect1">
          <option>{show_20}</option>
          <option>{show_30}</option>
          <option>{show_40}</option>
        </select>
      </div>
    </div>
  );
}

export default PerPage;
