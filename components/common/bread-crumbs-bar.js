import React from "react";
import useTranslation from "../../services/use-translation";

function BreadcrumbsBar() {
  const {your_cart , home , library , data} = useTranslation();
  return (
      <div className="breadcrumb-container">
        <h1>{your_cart}</h1>
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <a href="#">{home}</a>
          </li>
          <li className="breadcrumb-item">
            <a href="#">{library}</a>
          </li>
          <li className="breadcrumb-item active" aria-current="page">
          {data}
          </li>
        </ol>
      </div>
  );
}

export default BreadcrumbsBar;
