import React from "react";
import useTranslation from "../../services/use-translation";

function Backtop(){
	const {back_to_top} = useTranslation();
	return (
		<div className="backtotop">
			<a href="#">{back_to_top}</a>
		</div>
	);
}

export default Backtop;