import React from "react";
import Head from "next/head";
import useTranslation from "../../services/use-translation";

function PageHead() {
	const { mot_title } = useTranslation();
	return (
		<Head>
			<meta charSet="utf-8" />
			<link rel="icon" href="/favicon.ico" />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<meta name="theme-color" content="#000000" />
			<meta name="description" content="Web site created using create-react-app" />
			<link rel="apple-touch-icon" href="/logo192.png" />
			<title>{mot_title}</title>

			<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
			<link href="/assets/css/style.css" rel="stylesheet" />
			<link href="/assets/css/responsive.css" rel="stylesheet" />
			<link rel="stylesheet" href="/carousel/owl.carousel.min.css" />

			<link href="/assets/css/theme_red.css" rel="stylesheet" id="pagestyle" />
			<link rel="stylesheet" type="text/css" href="https://coreui.io/v1/demo/AngularJS_Demo/vendors/css/simple-line-icons.min.css" />
			<link rel="stylesheet" type="text/css" href="https://coreui.io/v1/demo/AngularJS_Demo/vendors/css/font-awesome.min.css" />
		</Head>
	);
}

export default PageHead;
