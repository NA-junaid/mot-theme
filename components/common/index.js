import Backtop from './back-top';
import BreadcrumbsBar from './bread-crumbs-bar';
import Counter from './counter';
import Footer from './footer';
import GridItem from './grid-item';
import Newsletter from './news-letter';
import PageHead from './page-head';

export {Backtop,BreadcrumbsBar,Counter,Footer,GridItem,Newsletter,PageHead};