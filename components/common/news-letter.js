import React from "react";
import useTranslation from "../../services/use-translation";

function Newsletter() {
    const { sub_new_text, enter_your_email, subscribe_now } = useTranslation();
    return (
        <section className="newsletter">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="content">
                            <form>
                                <h2>{sub_new_text}</h2>
                                <div className="input-group">
                                    <input type="email" className="form-control" placeholder={enter_your_email} />
                                    <span className="input-group-btn">
                                        <button className="btn" type="submit">{subscribe_now}</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Newsletter;