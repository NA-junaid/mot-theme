import React from "react";
import useTranslation from "../../services/use-translation";

function Footer() {
  const { customer_services, help_Center, contact_us, report_abuse, submit_dispute, policies_rules, get_Paid_feedback, mot_b2c, mot_b2b, trade_services, top_brands, all_rights , designed_by} = useTranslation();
  return (
    <section className="footer">
      <div className="container">
        <div className="row text-center text-xs-center text-sm-left text-md-left">
          <div className="col-xs-12 col-sm-3 col-md-3">
            <h5>{customer_services}</h5>
            <ul className="list-unstyled quick-links">
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{help_Center}
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{contact_us}
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{report_abuse}
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{submit_dispute}
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{policies_rules}
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>{get_Paid_feedback}
                </a>
              </li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <h5>{mot_b2c}</h5>
            <ul className="list-unstyled quick-links">
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Bags &amp; Shoes
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Construction Tools
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Food, Beverage
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Health &amp; Beauty
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Home Stuff
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Jewelry, Watch
                </a>
              </li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <h5>{mot_b2b}</h5>
            <ul className="list-unstyled quick-links">
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Products &amp;
                  Services (B2B)
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Buying Requests
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Members
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Directory of Turkish
                  Companies
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Exporters in Turkey
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Membership Packages
                </a>
              </li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-3 col-md-3">
            <h5>{trade_services}</h5>
            <ul className="list-unstyled quick-links">
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Trade Assurance
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Business Identity
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i> Logistics Service
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-angle-double-right"></i>Production Monitoring{" "}
                </a>
              </li>
            </ul>
            <div className="social_icons">
              <a href="#">
                <i className="fa fa-facebook"></i>
              </a>
              <a href="#">
                <i className="fa fa-twitter"></i>
              </a>
              <a href="#">
                <i className="fa fa-linkedin"></i>
              </a>
              <a href="#">
                <i className="fa fa-instagram"></i>
              </a>
            </div>
          </div>
        </div>

        <div className="top_brands">
          <h2>{top_brands}</h2>
          <a href="#">Realme </a>
          <a href="#">X2Realme </a>
          <a href="#">5Infinix Hot </a>
          <a href="#">8iPhone </a>
          <a href="#">11iPhone 11 Pro</a>
          <a href="#">iPhone 11 Pro Max</a>
          <a href="#">Mobile Offers</a>
          <a href="#">iphone x</a>
          <a href="#">iphone 8</a>
          <a href="#">iphone 7</a>
          <a href="#">iphone 6</a>
          <a href="#">Redmi 5</a>
          <a href="#">A4G Mobile</a>
          <a href="#">Nokia Mobile</a>
          <a href="#">Samsung Mobile</a>
          <a href="#">iphoneOppo </a>
          <a href="#">MobileVivo Mobile</a>
          <a href="#">GoPro Action Camera</a>
          <a href="#">Nikon Camera</a>
          <a href="#">Canon Camera</a>
          <a href="#">Sony Camera</a>
          <a href="#">Canon DSLR</a>
          <a href="#">Nikon DSLR</a>
          <a href="#">Realme </a>
          <a href="#">X2Realme </a>
          <a href="#">5Infinix Hot </a>
          <a href="#">8iPhone </a>
          <a href="#">11iPhone 11 Pro</a>
          <a href="#">iPhone 11 Pro Max</a>
          <a href="#">Mobile Offers</a>
          <a href="#">iphone x</a>
          <a href="#">iphone 8</a>
          <a href="#">iphone 7</a>
          <a href="#">iphone 6</a>
          <a href="#">Redmi 5</a>
          <a href="#">A4G Mobile</a>
          <a href="#">Nokia Mobile</a>
          <a href="#">Samsung Mobile</a>
          <a href="#">iphoneOppo </a>
          <a href="#">MobileVivo Mobile</a>
          <a href="#">GoPro Action Camera</a>
          <a href="#">Nikon Camera</a>
          <a href="#">Canon Camera</a>
          <a href="#">Sony Camera</a>
          <a href="#">Canon DSLR</a>
          <a href="#">Nikon DSLR</a>
          <a href="#">Realme </a>
          <a href="#">X2Realme </a>
          <a href="#">5Infinix Hot </a>
          <a href="#">8iPhone </a>
          <a href="#">11iPhone 11 Pro</a>
          <a href="#">iPhone 11 Pro Max</a>
          <a href="#">Mobile Offers</a>
          <a href="#">iphone x</a>
          <a href="#">iphone 8</a>
          <a href="#">iphone 7</a>
          <a href="#">iphone 6</a>
          <a href="#">Redmi 5</a>
          <a href="#">A4G Mobile</a>
          <a href="#">Nokia Mobile</a>
          <a href="#">Samsung Mobile</a>
          <a href="#">iphoneOppo </a>
          <a href="#">MobileVivo Mobile</a>
          <a href="#">GoPro Action Camera</a>
          <a href="#">Nikon Camera</a>
          <a href="#">Canon Camera</a>
          <a href="#">Sony Camera</a>
          <a href="#">Canon DSLR</a>
          <a href="#">Nikon DSLR</a>
        </div>

        <div className="text-center mt-4 pt-3">
          <img src="assets/img/home_products/associates.jpg" />
        </div>
        <div className="text-center mt-4">
          <img src="assets/img/home_products/payment.jpg" />
        </div>

        <div className="text-center mt-4 copyright">
          <p>{all_rights}</p>
        </div>

        <div className="text-center mt-3">
          <p className="designedby">{designed_by}</p>
        </div>
      </div>
    </section>
  );
}

export default Footer;
