import React from "react";
import Link from 'next/link';
import useTranslation from "../../services/use-translation";

function GridItem() {
  const {buy} = useTranslation();
  return (
    <div className="col-md-4">
      <div className="products_wrapper">
        <div className="hover_state">
          <h4>Fast Store</h4>
          <div className="cart">
            <Link href="/product" className="view">
              <a>
                <i className="icon-eye" />
              </a>
            </Link>
            <a href="#">
              <i className="icon-basket" /> {buy}
            </a>
            <a href="#" className="wishlist">
              <i className="icon-heart" />
            </a>
            <a href="#" className="compare">
              <i className="icon-shuffle" />
            </a>
            <h2>Jordan Galaxy</h2>
            <h3 className="price mt-2">KD 280</h3>
          </div>
        </div>
        <figure>
          <img src="assets/img/products/img1.jpg" />
        </figure>
        <h2>Jordan Galaxy</h2>
        <h3 className="price mt-2">KD 280</h3>
      </div>
    </div>
  );
}

export default GridItem;
