import React from "react";

function Script() {
  return (
    <div>
        <script src="lib/jquery/jquery.min.js"></script>
        <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="carousel/jquery.min.js"></script>
        <script src="carousel/owl.carousel.js"></script>
        <script>
        {/* function swapStyleSheet(sheet){
            document.getElementById('pagestyle').setAttribute('href', sheet);
        } */}
        </script>
    </div>
  );
}

export default Script;
